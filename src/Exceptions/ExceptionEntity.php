<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Exceptions;

/**
 * Ошибка сохранения объекта
 *
 * Class ExceptionEntity
 * @package Iswin\Borm\Exception
 */
class ExceptionEntity extends \Exception
{
    protected $data;

    /**
     * Возвращает поля объекта на котором произошла ошибка
     *
     * @return mixed
     */
    public function getData ()
    {
        return $this->data;
    }

    /**
     * Устанавливает поля объекта на котором произошла ошибка
     *
     * @param $data
     * @return $this
     */
    public function setData ($data)
    {
        $this->data = $data;
        return $this;
    }
}