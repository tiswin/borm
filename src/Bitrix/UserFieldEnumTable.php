<?php
/**
 * Created by Iswin.
 * Date: 09.05.18
 * Time: 11:47
 */


namespace Iswin\Borm\Bitrix;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity;

/**
 * Модель значений свойств типа списка для свойств пользовательского тип.
 *
 * Class UserFieldEnumTable
 * @package Iswin\Borm\Bitrix
 */
class UserFieldEnumTable extends DataManager
{
    /**
     * @see \Bitrix\Main\Entity\DataManager::getTableName()
     *
     * @return string
     */
    public static function getTableName ()
    {
        return 'b_user_field_enum';
    }

    /**
     * @see \Bitrix\Main\Entity\DataManager::getMap()
     *
     * @return array
     */
    public static function getMap ()
    {
        return [
            new Entity\IntegerField('ID', ['primary' => true, 'autocomplete' => true]),
            new Entity\IntegerField('USER_FIELD_ID'),
            new Entity\ReferenceField(
                'USER_FIELD',
                '\Bitrix\Main\UserField',
                ['=this.USER_FIELD_ID' => 'ref.ID']
            ),
            new Entity\StringField('VALUE'),
            new Entity\StringField('DEF'),
            new Entity\IntegerField('SORT'),
            new Entity\StringField('XML_ID')
        ];
    }
}