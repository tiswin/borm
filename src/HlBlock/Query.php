<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock;

use Bitrix\Main\Entity\Query as BitrixEntityQuery;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity\Field;
use Iswin\Borm\Common\EntityInterface;
use Iswin\Borm\Common\QueryInterface;
use Iswin\Borm\HlBlock\References\IblockReference;
use Iswin\Borm\HlBlock\References\IblockReferenceManager;

Loader::includeModule('highloadblock');

/**
 * Класс наследник Bitrix\Main\Entity\Query, позволяет в fetch получать не массивы,
 * а объекты сущностей (наследники класса - Iswin\Borm\HlBlock\HighLoadEntity.
 *
 * Class Query
 * @package Iswin\Borm\HlBlock
 */
class Query extends BitrixEntityQuery implements QueryInterface
{

    /**
     * регулярное выражение для проверки ключа связанной модели в select
     */
    const REFERENCE_KEY_CHECK_PREG = "#^([^\.]+)\.(.+)$#Umsi";
    /**
     * регулярное выражения для разбивки на слова Camel Case строки
     */
    const SPLIT_CAMEL_CASE_PREG = '/((?:^|[A-Z])[a-z]+)/';

    protected $referenceMapByName = [];
    protected $referenceMapByClass = [];
    protected $selectReferences = [];
    protected $iblockReferences = [];


    public function __construct ($source)
    {
        parent::__construct($source);
        $this->fillReferenceMap();
    }

    protected function fillReferenceMap()
    {
        $fields = static::getEntity()->getDataClass()::getMap();
        /** @var Field|ReferenceField $field */
        foreach ($fields as $field) {
            if (!($field instanceof ReferenceField) || $field instanceof IblockReference) {
                continue;
            }

            $referenceClassName = $field->getRefEntity()->getDataClass();
            $this->referenceMapByName[$field->getName()] = $referenceClassName;
            $this->referenceMapByClass[$referenceClassName] = $field->getName();
        }
    }

    /**
     * @see QueryInterface::getEntityObject()
     *
     * @return HighLoadEntity
     */
    public function getEntityObject()
    {
        /** @var HighLoadEntity $dataClass */
        $dataClass = static::getEntity()->getDataClass();
        return $dataClass::getInstance();
    }

    protected $assocMap = false;

    protected function getAssocMap()
    {
        if ($this->assocMap !== false) {
            return $this->assocMap;
        }

        /** @var HighLoadEntity $dataClass */
        $dataClass = static::getEntity()->getDataClass();
        $assocMap = $dataClass::getAssocMap();

        return $this->assocMap = $assocMap;
    }

    protected function getCustomSelect($definition, $alias)
    {
        $assocMap = $this->getAssocMap();
        if (!isset($assocMap[$definition])) {
            return false;
        }

        $field = $assocMap[$definition];
        if (!($field instanceof IblockReference)) {
            return false;
        }

        /**
         * Если это связь с инфоблоком то в алиасе у нас query на выбор объектов инфоблока
         */
        /** @var \Iswin\Borm\Iblock\Query $query */
        $query = $alias;


        $iblockRefSettings = IblockReferenceManager::getInstance($field, $query);
        $fields = $iblockRefSettings->getHlKeys();

        foreach ($fields as $hlField) {
            parent::addSelect($hlField);
            if (!isset($this->iblockReferences[$hlField])) {
                $this->iblockReferences[$hlField] = [];
            }

            $this->iblockReferences[$hlField][] = $iblockRefSettings;
        }

        return true;
    }


    /**
     * @see \Bitrix\Main\Entity\Query::addSelect()
     *
     * @param mixed $definition
     * @param string $alias
     * @return $this
     */
    public function addSelect ($definition, $alias = '')
    {
        if ($this->getCustomSelect($definition, $alias)) {
            return $this;
        }

        $returnParent = parent::addSelect($definition, $alias);

        if (preg_match(static::REFERENCE_KEY_CHECK_PREG, $definition, $matches)) {

            $referenceCode = $matches[1];
            $field = $matches[2];

            if (!isset($this->referenceMapByName[$referenceCode])) {
                return $returnParent;
            }

            $className = $this->referenceMapByName[$referenceCode];

            if (strlen($alias) == 0 || !$alias) {
                $alias = $this->getReferenceAlias($referenceCode);
                if (!$alias) {
                    return $returnParent;
                }

                $alias .= '_';
            }

            $this->selectReferences[$alias] = [
                'class' => $className,
                'field' => $field
            ];
        }

        return $returnParent;
    }

    /**
     * @param array $select
     * @return $this
     */
    public function setSelect (array $select)
    {
        foreach ($select as $key => $alias) {
            if (!is_numeric($key)) {
                $this->addSelect($key, $alias);
            } else {
                $this->addSelect($alias);
            }
        }

        return $this;
    }

    protected function getReferenceAlias($referenceCode)
    {
        if (!isset($this->referenceMapByName[$referenceCode])) {
            return false;
        }

        $alias = $this->classNameToKey(static::getEntity()->getDataClass()) . "_{$referenceCode}";
        return $alias;
    }



    /**
     * Переопределение Bitrix\Main\Entity\Query::query() с целью добавления
     * своего fetch - модификатора
     *
     * @param $query
     * @return Result
     */
    public function query ($query)
    {
        $result = parent::query($query);
        if (is_subclass_of($this->getEntity()->getDataClass(), '\Iswin\Borm\HlBlock\HighLoadEntity')) {
            $result->addFetchDataModifier([$this, 'fetchHighLoadEntity']);
        }
        return new Result($result, $this->iblockReferences);
    }


    /**
     * @see QueryInterface::fetchAll()
     *
     * @return EntityInterface[]
     */
    public function fetchAll()
    {
        return $this->exec()->fetchAll();
    }


    /**
     * @return Result
     */
    public function exec ()
    {
        $select = $this->getSelect();
        if (!in_array('ID', $select)) {
            $this->addSelect('ID');
        }

        return parent::exec();
    }

    /**
     * @param string $definition
     * @param string $order
     * @return $this
     */
    public function addOrder ($definition, $order = 'ASC')
    {
        return parent::addOrder($definition, $order);
    }

    /**
     * @param mixed $order
     * @return $this
     */
    public function setOrder ($order)
    {
        return parent::setOrder($order);
    }

    /**
     * @param array $filter
     * @return $this
     */
    public function setFilter (array $filter)
    {
        return parent::setFilter($filter);
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    public function addFilter ($key, $value)
    {
        return parent::addFilter($key, $value);
    }

    /**
     * @param mixed $group
     * @return $this
     */
    public function setGroup ($group)
    {
        return parent::setGroup($group);
    }

    /**
     * @param $group
     * @return $this
     */
    public function addGroup ($group)
    {
        return parent::addGroup($group);
    }

    /**
     * @param int $limit
     * @return $this
     */
    public function setLimit ($limit)
    {
        return parent::setLimit($limit);
    }

    /**
     * @param int $offset
     * @return $this
     */
    public function setOffset ($offset)
    {
        return parent::setOffset($offset);
    }


    protected static $cacheKeysClass = [];

    /**
     * Обратное преобразование название класса сущности, в ключ выборки (см. keyToClassName() )
     *
     * @param $className
     * @return string
     */
    protected function classNameToKey($className) {
        if (isset(self::$cacheKeysClass[$className])) {
            return self::$cacheKeysClass[$className];
        }

        $ar = explode('\\', $className);
        $last = array_pop($ar);
        preg_match_all(static::SPLIT_CAMEL_CASE_PREG,$last,$matches);
        foreach ($matches[1] as $word) {
            $ar[] = $word;
        }

        $ret = [];
        foreach ($ar as $word) {
            if (!$word) {
                continue;
            }
            if ($word == 'Table') {
                continue;
            }
            $ret[] = strtoupper($word);
        }

        return self::$cacheKeysClass[$className] = implode('_', $ret);
    }

    protected static $cacheParseValue = [];


    /**
     * Вспомогательный метод для ассоциации ключей массива данных элемента
     * к соответсвующим объектам
     *
     * @param string $className
     * @param string $classKey
     * @param string $key
     * @return array|bool
     */
    protected function parseValue($className, $classKey, $key) {

        if (isset(self::$cacheParseValue[$className][$classKey][$key])) {
            return self::$cacheParseValue[$className][$classKey][$key];
        }

        $selectedAliases = array_keys($this->selectReferences);

        if (!$selectedAliases) {
            return false;
        }

        $preg = "#^(" . implode('|', $selectedAliases) . ")(.*)$#Umsi";
        if (!preg_match($preg, $key, $matches)) {
            return self::$cacheParseValue[$className][$classKey][$key] = false;
        }

        $ref = $matches[1];
        $field = $matches[2];
        if (!isset($this->selectReferences[$ref])) {
            return self::$cacheParseValue[$className][$classKey][$key] = false;
        }

        $selectReference = $this->selectReferences[$ref];

        $ret = self::$cacheParseValue[$className][$classKey][$key] = [
            'base_key' => $this->referenceMapByClass[$selectReference['class']],
            'key' => $ref,
            'field' => $selectReference['field'] == '*' ? $field : $selectReference['field'],
            'class' => $selectReference['class']
        ];

        return $ret;
    }


    protected static $arrayReferenceCache = [];

    /**
     * @todo плохой метод, но для текущих задач хватает, но переделать нужно
     *
     * @param $key1
     * @param $key2
     * @return bool
     */
    protected function isArrayRef($key1, $key2)
    {
        list($link1, $val1) = explode('.', $key1);
        list($link2, $val2) = explode('.', $key2);

        $link1 = str_replace("=", "", $link1);
        $link2 = str_replace("=", "", $link2);

        return
            ($link1 == 'this' && $val1 == 'ID') ||
            ($link2 == 'this' && $val2 == 'ID');
    }

    /**
     * @param string $entityClassName
     * @param HighLoadEntity $referenceClassName
     * @return mixed
     */
    protected function isArrayValue($entityClassName, $referenceClassName)
    {

        if (isset(self::$arrayReferenceCache[$entityClassName][$referenceClassName])) {
            return self::$arrayReferenceCache[$entityClassName][$referenceClassName];
        }

        $mapItems = $entityClassName::getMap();
        /** @var Field|ReferenceField $mapItem */
        foreach ($mapItems as $mapItem) {
            if (!($mapItem instanceof ReferenceField)) {
                continue;
            }
            $refClassName = $mapItem->getRefEntityName() . "Table";
            $refs = $mapItem->getReference();
            foreach ($refs as $key1 => $key2) {
                if ($this->isArrayRef($key1, $key2)) {
                    self::$arrayReferenceCache[$entityClassName][$refClassName] = true;
                } else {
                    self::$arrayReferenceCache[$entityClassName][$refClassName] = false;
                }
            }

        }

        return self::$arrayReferenceCache[$entityClassName][$referenceClassName];
    }

    /**
     * подготавливает массив данных элемента, для преобразования в объект соответсвующей модели данных
     *
     * @param array $data - массив данных элемента
     * @return array
     */
    protected function parseRow(&$data) {

        $links =[];
        $classNameMap = [];
        $className =  $entityClassName = $this->getEntity()->getDataClass();
        $classKey = $this->classNameToKey($className);

        foreach ($data as $key => $val) {

            $parseData = $this->parseValue($className, $classKey, $key);
            if (!$parseData) {
                continue;
            }

            $skip = true;

            if (!isset($links[$parseData['base_key']])) {
                $links[$parseData['base_key']] = [];
                $classNameMap[$parseData['base_key']] = $parseData['class'];
            }
            $links[$parseData['base_key']][$parseData['field']] = $val;
        }

        if ($links) {
            foreach ($links as $key => $d) {
                $className = $classNameMap[$key];

                $skip = true;
                foreach ($d as $v) {
                    if (!is_null($v)) {
                        $skip = false;
                    }
                }

                if ($skip) {
                    continue;
                }

                $class = new $className($d);
                if ($class instanceof HighLoadEntityInterface) {
                    if ($this->isArrayValue($entityClassName, "\\" . get_class($class))) {
                        $data[$key] = [new $className($d)];
                    } else {
                        $data[$key] = new $className($d);
                    }

                } else {
                    $data[$key] = $d;
                }
            }
        }

        return $data;
    }

    /**
     * Кастомный фетчер данных (вызывается в цикле для каждого элемента в выборке),
     * преобразует массив данных элементов, в объект соответствующей модели данных.
     *
     * @param array $data - данные элемента
     * @return array
     */
    public function fetchHighLoadEntity(&$data) {
        $className = $this->getEntity()->getDataClass();
        $data = $this->parseRow($data);
        $data['_ENTITY'] = new $className($data);
        return $data;
    }
}