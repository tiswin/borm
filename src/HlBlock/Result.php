<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock;

use Bitrix\Main\Loader;
use Iswin\Borm\Common\EntityInterface;
use Iswin\Borm\HlBlock\References\IblockReferenceManager;

Loader::includeModule('highloadblock');

/**
 * Класс перехватывает вызовы методов классов \Bitrix\Main\DB\ArrayResult, \Bitrix\Main\DB\Result,
 * для подмены результата выборки (подменяем массивы на объекты)
 *
 * Class Result
 * @package Iswin\Borm\HlBlock
 */
class Result
{
    protected $realResult;
    protected $rows = [];
    protected $iblockReferences = [];

    /**
     * Result constructor.
     * @param \Bitrix\Main\DB\ArrayResult|\Bitrix\Main\DB\Result $result
     */
    public function __construct ($result, $iblockReferences = [])
    {
        $this->realResult = $result;
        $this->iblockReferences = $iblockReferences;
        $this->fillRows();
    }

    protected function fillRows ()
    {
        $this->rows = $this->getMerged();
    }

    /**
     * @return EntityInterface[]
     */
    public function fetchAll()
    {
        return $this->rows;
    }


    /**
     * Если в выборке участвуют связи типа one-to-many или many-to-many,
     * то при обычной обработке у нас будет для каждой комбинации таких связей появляться дополнительный элемент,
     * с совпадающим ID. Поэтому если мы добавляем в select связи таких типов, обязательно использовать метод getMerged()
     * вместо fetch. Однако остерегайтесь добавление в select нескольких связей типа one-to-many или many-to-many, в данном случае
     * могут возникнуть проблемы.
     *
     * @return HighLoadEntity[]
     */
    protected function getMerged ()
    {
        /** @var HighLoadEntity[] $ret */
        $ret = [];
        /** @var HighLoadEntity $row */
        while ($row = $this->realFetch()) {
            $id = $row->getId();
            if (!isset($ret[$id])) {
                $ret[$id] = $row;
            } else {
                $this->mergeRows($ret[$id], $row);
            }

            $rowData = $row->getData();
            foreach ($this->iblockReferences as $key => $iblockRefManagers) {
                if (!isset($rowData[$key])) {
                    continue;
                }

                $value = $rowData[$key];
                /** @var IblockReferenceManager $iblockRefManager */
                foreach ($iblockRefManagers as $iblockRefManager) {
                    $iblockRefManager->addValue($ret[$id], $key, $value);
                }
            }

            $ret[$id]->setStatusLoaded();
        }



        if ($this->iblockReferences) {
            $runningManagers = [];
            foreach ($this->iblockReferences as $key => $iblockRefManagers) {
                /** @var IblockReferenceManager $iblockRefManager */
                foreach ($iblockRefManagers as $iblockRefManager) {
                    $classId = spl_object_hash($iblockRefManager);
                    /**
                     * т.к. менеджеры связи у нас ассоциированы по ключам полей
                     * (для быстроты операции связывания на больших объемах), то обход по такому массиву может вызвать
                     * запуск линковки объектов несколько раз для одного и того же объекта, кодом ниже
                     * мы защищаемся от этого
                     */
                    if (isset($runningManagers[$classId])) {
                        continue;
                    }

                    $runningManagers[$classId] = $iblockRefManager->exec();
                }
            }
        }

        return $ret;
    }

    protected function realFetch ()
    {
        $row = $this->realResult->fetch();
        return $row['_ENTITY'];
    }

    /**
     * Переопредляем метод fetch, чтобы он вернул нам не массив как обычно, а объект,
     * сформированный в классе query.
     *
     * @see \Bitrix\Main\DB\Result::fetch()
     *
     * @return HighLoadEntity
     */
    public function fetch() {
        $row = current($this->rows);
        next($this->rows);

        if (!$row) {
            reset($this->rows);
        }

        return $row;
    }

    /**
     * @param HighLoadEntity[] $oldArray
     * @param HighLoadEntity[] $newArray
     * @return HighLoadEntity[]
     */
    protected function mergeHlEntitiesArray($oldArray, $newArray)
    {
        $mapOld = [];
        $oldEntity = false;
        foreach ($oldArray as $oldEntity) {
            if ($oldEntity instanceof HighLoadEntityInterface) {
                $mapOld[$oldEntity->getId()] = $oldEntity;
            }
        }

        foreach ($newArray as $newEntity) {
            if ($oldEntity instanceof HighLoadEntityInterface) {
                if ($newEntity instanceof HighLoadEntityInterface) {
                    $mapOld[$newEntity->getId()] = $newEntity;
                }
            }
        }

        return array_values($mapOld);
    }

    protected function mergeRows(HighLoadEntityInterface $rowExists, HighLoadEntityInterface $newRow)
    {
        $existsData = $rowExists->getData();
        $newData = $newRow;

        foreach ($existsData as $key => $val) {

            $newVal = $newValId = $newData[$key];

            if (is_array($newVal)) {
                $oldVal = $rowExists->getField($key);
                if (!is_array($oldVal)) {
                    $oldVal = [];
                }

                if (!$newVal) {
                    continue;
                }

                $rowExists->setField($key, $this->mergeHlEntitiesArray($oldVal, $newVal));

                continue;
            }

            $oldVal = $val;

            if (is_array($val)) {
                if ($newVal instanceof HighLoadEntityInterface) {
                    $newValId = $newVal->getId();
                    $exists = $val;
                    $val = [];
                    /** @var HighLoadEntityInterface $entity */
                    foreach ($exists as $entity) {
                        $val[] = $entity->getId();
                    }
                }

                if (!in_array($newValId, $val) && $newValId) {
                    $oldVal[] = $newVal;
                    $rowExists->setField($key, $oldVal);
                }

            } elseif ($val instanceof HighLoadEntityInterface) {
                if ($newVal instanceof HighLoadEntityInterface) {
                    if ($val->getId() != $newVal->getId()) {
                        $fieldData = [$val, $newVal];
                        $rowExists->setField($key, $fieldData);
                    }
                }
            } else {
                if ($val != $newVal) {
                    $fieldData = [$val, $newVal];
                    $rowExists->setField($key, $fieldData);
                }
            }
        }
    }

    public function __call($name, $arguments) {
        return call_user_func_array(array($this->realResult, $name), $arguments);
    }

    public function __set($name, $value)
    {
        $this->realResult->$name = $value;
    }

    public function __get ($name)
    {
        return $this->realResult->$name;
    }
}