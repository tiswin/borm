<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock\References;

use Bitrix\Main\Entity\ReferenceField;

/**
 * Class IblockReference
 * @package Iswin\Borm\HlBlock\References
 */
class IblockReference extends ReferenceField
{
}