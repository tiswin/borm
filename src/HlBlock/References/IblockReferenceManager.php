<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock\References;

use Bitrix\Main\Entity\ReferenceField;
use Iswin\Borm\Common\EntityInterface;
use Iswin\Borm\Common\QueryInterface;
use Iswin\Borm\HlBlock\HighLoadEntity;
use Iswin\Borm\Iblock\IblockEntity;

/**
 * Воспомогательный класс для связывания сущностей инфоблока с hl сущностей,
 * через соответствующие свойства
 *
 * Пример задания связи с элементом инфоблока:
 * <code>
 *      public static function getMap() {
 *          return [
 *              //....
 *              new IblockReference(
 *                  'PUBLIC',
 *                  '\Vendor\Module\Entities\Iblock\Product',
 *                  array('=this.UF_PUBLIC_ID' => 'ref.ID')
 *              )
 *              //....
 *          ];
 *      }
 * </code>
 *
 * Пример выбокри связанного инфоблока:
 * <code>
 *         $offer = OfferTable::query()
 *              ->addSelect('ID')
 *              ->addSelect('UF_PRODUCT_ID')
 *              ->addSelect(
 *                  'PRODUCT',
 *                  Product::query()
 *                  ->selectCatalog()
 *              )
 *              ->addFilter('=ID', $productId)
 *              -exec()
 *              ->fetch();
 * </code>
 *
 * Class IblockReferenceManager
 * @package Iswin\Borm\HlBlock\References
 */
class IblockReferenceManager
{

    /**
     * Максимальный шаг выборки
     * @todo пока не используется
     * @see IblockReferenceManager::getValuesForQuery()
     */
    const MAX_STEP_SELECT = 500;

    /**
     * @var ReferenceField
     */
    protected $field;
    /**
     * @var QueryInterface
     */
    protected $query;
    protected $filterMap = false;
    protected $values = [];
    /**
     * Соответсвия названия полей связанных инфоблоков с hl сущностями
     * <code>
     *     $ar = [
     *          'iblock_field_name' => 'hl_field_name'
     *      ];
     * </code>
     *
     * @var array
     */
    protected $linkedFieldsMap = [];

    /**
     * Соответсвия названия полей hl объектов с полями связанных инфоблоков
     * <code>
     *     $ar = [
     *          'hl_field_name' => 'iblock_field_name'
     *      ];
     * </code>
     *
     * @var array
     */
    protected $linkedFieldsMapRevers = [];
    protected $linkedEntities = [];

    protected function __construct (ReferenceField $field, QueryInterface $query)
    {
        $this->field = $field;
        $this->query = $query;
        $this->filterMap = $this->getFilterMap();
    }

    /**
     * Возвращает инстанс настроек
     *
     * @param ReferenceField $field - настройки связи (возвращаемый методом getMap - hl объекта)
     * @param QueryInterface $query - настройки выборки связанного инфоблока
     *
     * Пример (выборка проудкта хранящегося в hl блоках, вместе со связанным с ним элемента инфоблока через UF_BRAND)
     * <code>
     *         $product = ProductTable::query()
     *              ->addSelect('ID')
     *              ->addSelect('UF_BRAND')
     *              ->addSelect(
     *                  'BRAND',
     *                  Product::query()
     *                  ->selectCatalog()
     *              )
     *              ->addFilter('=ID', $productId)
     *              -exec()
     *              ->fetch();
     * </code>
     * @return IblockReferenceManager
     */
    public static function getInstance(ReferenceField $field, QueryInterface $query)
    {
        return new IblockReferenceManager($field, $query);
    }

    /**
     * @return ReferenceField
     */
    protected function getField()
    {
        return $this->field;
    }

    protected function getFilterMap()
    {
        if ($this->filterMap !== false) {
            return $this->filterMap;
        }

        $reference = $this->getField()->getReference();
        $filter = [];
        foreach ($reference as $key1 => $key2) {
            list($link1, $val1) = explode('.', $key1);
            list($link2, $val2) = explode('.', $key2);

            $link1 = str_replace("=", "", $link1);
            $link2 = str_replace("=", "", $link2);

            if ($link1 == 'this') {
                $filter["={$val2}"] = $val1;
                $this->values[$val1] = [];
                $this->linkedFieldsMap[$val2] = $val1;
            }

            if ($link2 == 'this') {
                $filter["={$val1}"] = $val2;
                $this->values[$val2] = [];
                $this->linkedFieldsMap[$val1] = $val2;
            }
        }

        $this->linkedFieldsMapRevers = array_flip($this->linkedFieldsMap);

        return $this->filterMap = $filter;
    }

    /**
     * Возвращает массив полей hl сущности, используемых для выборки связанного объекта инфоблока
     *
     * @return array
     */
    public function getHlKeys()
    {
        $map = $this->getFilterMap();
        return array_values($map);
    }

    /**
     * Добавляет значение поля hl объекта в фильтр для последующей выборки связанных объектов инфоблока
     *
     * @param EntityInterface $entity - hl объект связанный с добавляемым значением
     * @param $key - ключ значения
     * @param $val - значение
     * @return $this
     */
    public function addValue(EntityInterface $entity, $key, $val)
    {

        if (!isset($this->values[$key])) {
            return $this;
        }

        /**
         * @todo подумать: т.к. выбираем связанные инфоблоки, то связывающее поле hl блока не может быть null или false
         */
        if (!$val) {
            return $this;
        }

        $this->values[$key][] = $val;

        $val = $val ? : '0';

        $vals = $val;

        if (!is_array($vals)) {
            $vals = [$val];
        }

        $iblockField = $this->linkedFieldsMapRevers[$key];
        if (!isset($this->linkedEntities[$iblockField])) {
            $this->linkedEntities[$iblockField] = [];
        }

        foreach ($vals as $val) {
            if (!isset($this->linkedEntities[$iblockField][$val])) {
                $this->linkedEntities[$iblockField][$val] = [];
            }
            $this->linkedEntities[$iblockField][$val][] = $entity;
        }

        return $this;
    }

    /**
     * @param array $allValues
     * @return QueryInterface|bool
     */
    protected function getQuery($allValues)
    {
        $needQuery = false;
        /** @var QueryInterface $query */
        $query = $this->query;
        foreach ($this->filterMap as $expression => $hlField) {
            $values = $allValues[$hlField];
            if (!$values) {
                continue;
            }

            $query->addFilter($expression, $values);
            $needQuery = true;
        }
        /**
         * если ни одного фильтра не добавили, запрос запрещаем иначе выберуться все объекты инфоблока
         */
        if (!$needQuery) {
            return false;
        }

        return $query;
    }

    /**
     * @todo - неплохо бы сделать:
     * Т.к. в values для фильтра связанных объектов инфоблока
     * может быть достаточно много элементов, более того ключей для связи
     * тоже может быть несколько, то мы разбиваем массив для фильтрации на множество массивов
     * каждый из которых гарантирует что в выборке не будет использоваться элементов более чем
     * определено в self::MAX_STEP_SELECT
     *
     * @param $allValues
     * @return array
     */
    protected function getValuesForQuery($allValues)
    {
        return [$allValues];
    }

    /**
     * Осуществляет выборку связанных объектов инфоблока
     * и привязывает их к соответсвующим hl объектам.
     * Возвращает true - если выборка была осуществлена,
     * в противном случае false
     *
     * @return bool
     */
    public function exec()
    {
        if (!$this->values) {
            return false;
        }


        $valuesForQuery = $this->getValuesForQuery($this->values);

        $queryExec = false;
        $setField = $this->getField()->getName();

        foreach ($valuesForQuery as $values) {
            $query = $this->getQuery($values);
            if (!$query) {
                continue;
            }

            $queryExec = true;
            $rows = $query->fetchAll();
            /** @var IblockEntity $row */
            foreach ($rows as $row) {
                $data = $row->getData();
                foreach ($this->linkedEntities as $iblockField => $links) {
                    $hlField = $this->linkedFieldsMap[$iblockField];
                    $iblockVal = $data[$iblockField] ? : '0';
                    if (!isset($links[$iblockVal])) {
                        continue;
                    }

                    /** @var HighLoadEntity $entity */
                    foreach ($links[$iblockVal] as $entity) {
                        $entity->setField($setField, $row);
                    }
                }
            }
        }


        return $queryExec;
    }
}