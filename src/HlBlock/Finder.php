<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock;

use Iswin\Borm\Common\Finder as RepoFinder;

/**
 * Сервис для поиска сущностей элементов и разделов highload блоков
 *
 * Class Finder
 * @package  Iswin\Borm\HlBlock
 */
class Finder extends RepoFinder
{
    /**
     * Регулярное выражение для проверки имени класса таблицы hl
     */
    const PREG_CHECK_TABLE_CLASS = "#^(.*)Table$#Umsi";

    /**
     * путь к репозиториям инфоблоков относительно корня модуля
     */
    const ENTITIES_DIR = '/lib/entities/hlblock/';


    /**
     * @param $modulePath
     * @param $code
     */
    protected function searchInModule($modulePath, $code)
    {
        $modulePath .= static::ENTITIES_DIR;
        if (!file_exists($modulePath)) {
            return;
        }
        $this->recursiveSearchInPath($modulePath, $code);
    }

    /**
     * @param HighLoadEntity $className
     * @param $code
     */
    protected function checkResult($className, $code)
    {
        if (!method_exists($className, 'getTableName')) {
            return;
        }

        if (!preg_match(static::PREG_CHECK_TABLE_CLASS, $className)) {
            return;
        }


        if ($className::getTableName() == $code) {
            $this->result = $className::getInstance();
        }
    }

}