<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock;

use Bitrix\Main\Entity\UpdateResult;
use Bitrix\Main\Entity\AddResult;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Entity\Field;
use Bitrix\Main\Entity\Event;


/**
 * Класса позволяет сохранять все модели, с учетом их связей.
 * Class FlushManager
 * @package Iswin\Borm\HlBlock
 */
class FlushManager
{
    /**
     * @var HighLoadEntity
     */
    protected $entity;

    /**
     * Режим сохранения связей: связи не сохранять и не обрабатывать
     */
    const REFERENCE_NOT = 0;
    /**
     * Режим сохранения связей: устанавливать только ID связанной сущности, но не сохранять ее
     * предпологается, что все связанные сущности уже сохранены
     */
    const REFERENCE_ONLY_SET_ID = 1;
    /**
     * Режим сохранения связей: сохраняем связанную сущность, однако не ищем в ней и не обрабатываем
     * обратную ссылку на текущую
     */
    const REFERENCE_NOT_BACK_REFERENCE = 2;
    /**
     * Режим сохранения связей: сохраняем связанную сущность, а также ищем в ней и обрабатываем обратную
     * связь на текущую.
     * @deprecated
     */
    const REFERENCE_FULL_SAVE = 3;

    protected function __construct (HighLoadEntityInterface $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param HighLoadEntityInterface $entity
     * @return FlushManager
     */
    public static function getInstance(HighLoadEntityInterface $entity)
    {
        return new static($entity);
    }


    /**
     * Сохраняет объект, вместе с вложенными референсами
     *
     * Если в модели (или ее связях) не задан ID - создается новый объект, в противном случае происходит обновление объекта.
     *
     * Пример 1:
     *
     * код создаст по новому объекту OfferTable, ProductTable, ColorTable и свяжет их между собой
     * соответсвующим образом.
     * <code>
     *         $offer = new OfferTable(
     *                  [
     *                      'UF_NAME' => 'торговое предложение',
     *                      'UF_CODE' => 'test_offer',
     *                      'PRODUCT' => new ProductTable(
     *                          [
     *                              'UF_NAME' => 'тестовый продукт',
     *                          ]
     *                      ),
     *                      'COLOR' => new ColorTable(
     *                          [
     *                              'UF_NAME' => 'red',
     *                          ]
     *                      )
     *                  ]
     *          );
     *          HlEntityFlushManager::getInstance($offer)->flush();
     * </code>
     *
     * Класса позволяет сохранять все модели, с учетом их связей.
     *
     * Пример 2:
     *
     * код создаст по новому объекту OfferTable и ProductTable, а также обновит
     * объект ColorTable с ID = 12. Все связи между объектами сохранятся
     * и свяжет их между собой
     * соответсвующим образом.
     * <code>
     *          $color = ColorTable::getByID(12)->fetch();
     *          $color->setName('красный');
     *
     *         $offer = new OfferTable(
     *                  [
     *                      'UF_NAME' => 'торговое предложение',
     *                      'UF_CODE' => 'test_offer',
     *                      'COLOR' => $color,
     *                      'PRODUCT' => new ProductTable(
     *                          [
     *                              'UF_NAME' => 'тестовый продукт 2'
     *                          ]
     *                      )
     *                  ]
     *          );
     *          HlEntityFlushManager::getInstance($offer)->flush();
     * </code>
     *
     * Если в метод flush() передать параметр skipReference = true, то значение связанных объектов обработаны никак не будт.
     * В таком случае, в примерах выше, будет создан только объект OfferTable
     *
     * ВНИМАНИЕ: если для связанного объекта не будет выбран ID то будет создан новый элемент.
     *
     * Пример 3:
     *
     * Если не обеспечить в выборке ID связанного объекта, то при сохранении (без skipReference = true) будет создан новый
     * объект с полями которые были в выборке. Пример ниже приведет к тому, что вместо обновления ColorTable связанного с OfferTable
     * произойдет создание его копии, и OfferTable будет связан уже с вновь созданным объектом.
     *
     * <code>
     *      $offer = OfferTable::query()
     *          ->addSelect('*')
     *          ->addSelect('COLOR.UF_NAME')
     *          ->addFilter('=ID', 10)
     *          ->exec()
     *          ->fetch();
     *
     *      $offer->setName('Тестовое предложение');
     *      HlEntityFlushManager::getInstance($offer)->flush();
     * </code>
     *
     * Пример 4:
     *
     * Правильная выборка в отличии примера 3, если мы не хотим потерять связи с ShopTable
     *
     * <code>
     *      $offer = OfferTable::query()
     *          ->addSelect('*')
     *          ->addSelect('COLOR.ID')
     *          ->addSelect('COLOR.UF_NAME')
     *          ->addFilter('=ID', 10)
     *          ->exec()
     *          ->fetch();
     *
     *      $offer->setName('Тестовое предложение');
     *      HlEntityFlushManager::getInstance($offer)->flush();
     * </code>
     *
     *
     * @param int $referenceMode - режим обработки связанных сущностей
     * @see HlEntityFlushManager::REFERENCE_NOT
     * @see HlEntityFlushManager::REFERENCE_ONLY_SET_ID
     * @see HlEntityFlushManager::REFERENCE_NOT_BACK_REFERENCE
     * @see HlEntityFlushManager::REFERENCE_FULL_SAVE
     *
     * @return AddResult|UpdateResult
     */
    public function flush($referenceMode = 0)
    {
        $entity = $this->getEntity();
        static::markEntityAsFlushed($entity);

        $saveData = [];

        $entityId = $entity->getId();
        $data = $this->getEntity()->getData();
        $assocMap = $entity::getAssocMap();
        if ($referenceMode != static::REFERENCE_NOT) {
            /**
             * сохраним все связанные сущности
             */
            foreach ($data as $key => $val) {
                $field = $assocMap[$key];
                if (!$field) {
                    continue;
                }

                if ($field instanceof ReferenceField) {
                    if ($val instanceof HighLoadEntityInterface) {
                        $this->addReference($val, $field, $referenceMode);
                    }
                }
            }
        }

        /**
         * после того как связанные сущности сохранены, и данные в родительской обновлены
         * можно сохранять родительскую сущность
         */
        $data = $this->getEntity()->getData();
        foreach ($data as $key => $val) {
            if ($key == 'ID') {
                continue;
            }

            $field = $assocMap[$key];
            if (!$field) {
                continue;
            }

            if (!($field instanceof ReferenceField)) {
                $saveData[$key] = $val;
            }
        }

        static::setLastEntity($entity);

        if ($entityId) {
            $this->getEntity()->setStatusUpdated();
            $res = $entity::update($entityId, $saveData);
        } else {
            $this->getEntity()->setStatusAdded();
            $res = $entity::add($saveData);
        }
        return $res;
    }

    /**
     * @return HighLoadEntity
     */
    protected function getEntity()
    {
        return $this->entity;
    }

    protected function addReference(HighLoadEntityInterface $referenceObj, ReferenceField $field, $referenceMode)
    {
        if ($referenceMode >= static::REFERENCE_FULL_SAVE) {
            $mapItems = $referenceObj::getMap();
            /** @var Field $mapItem */
            foreach ($mapItems as $mapItem) {
                if ($mapItem instanceof ReferenceField) {
                    $refEntityClassName = $mapItem->getRefEntity()->getDataClass();
                    if ($refEntityClassName == '\\' . get_class($this->getEntity())) {
                        $mapItemName = $mapItem->getName();
                        $referenceObj->setField($mapItemName, $this->getEntity());
                    }
                }
            }
        }

        $referenceData = $referenceObj->getData();

        $refs = $field->getReference();
        $fields = [];

        foreach ($refs as $ref1 => $ref2) {
            if ($ref1[0] != '=') {
                continue;
            }
            $fieldsRow = $this->refsToFieldSettings(substr($ref1, 1), $ref2);
            if (!$fieldsRow) {
                continue;
            }

            if (isset($referenceData[$fieldsRow['ref']])) {
                $this->getEntity()->setField($fieldsRow['entity'], $referenceData[$fieldsRow['ref']]);
            } else {
                $fields[] = $fieldsRow;
            }
        }

        static::registerReference($referenceObj, $this->getEntity(), $fields);

        if ($referenceMode >= static::REFERENCE_NOT_BACK_REFERENCE) {
            if (!static::isFlushedEntity($referenceObj)) {
                static::getInstance($referenceObj)->flush();
            }
        }
    }

    protected function refsToFieldSettings($ref1, $ref2)
    {
        list($type1, $field1) = explode('.', $ref1);
        list($type2, $field2) = explode('.', $ref2);

        if ($type1 == 'this' && $type2 == 'ref') {
            $thisField = $field1;
            $refField = $field2;
        } elseif($type2 == 'this' && $type1 == 'ref') {
            $thisField = $field2;
            $refField = $field1;
        } else {
            return false;
        }

        return [
            'entity' => $thisField,
            'ref' => $refField
        ];
    }

    protected static $registerReferences = [];
    protected static $savedReference = [];
    protected static $flushedEntities = [];

    protected static function isFlushedEntity(HighLoadEntityInterface $entity)
    {
        $refHash = spl_object_hash($entity);
        return isset(self::$flushedEntities[$refHash]);
    }

    protected static function markEntityAsFlushed(HighLoadEntityInterface $entity)
    {
        $refHash = spl_object_hash($entity);
        self::$flushedEntities[$refHash] = true;
    }

    protected static function isRegisterReference(HighLoadEntityInterface $referenceObj)
    {
        $refHash = spl_object_hash($referenceObj);
        return isset(self::$registerReferences[$refHash]);
    }

    protected static function registerReference(HighLoadEntityInterface $referenceObj, HighLoadEntityInterface $entity, $fields)
    {
        $refHash = spl_object_hash($referenceObj);

        $row = [
            'object' => $entity,
            'fields' => []
        ];

        foreach ($fields as $field) {
            $row['fields'][$field['entity']] = $field['ref'];
        }

        self::$registerReferences[$refHash] = $row;
    }

    public static function resetRegisterReferences()
    {
        self::$registerReferences = [];
        self::$savedReference = [];
        self::$flushedEntities = [];
    }

    /**
     * @var HighLoadEntity
     */
    protected static $lastEntity;

    /**
     * @param HighLoadEntityInterface $entity
     */
    protected static function setLastEntity(HighLoadEntityInterface $entity)
    {
        self::$lastEntity = $entity;
    }

    /**
     * @return HighLoadEntity
     */
    protected static function getLastEntity()
    {
        return self::$lastEntity;
    }

    protected static function resetLastEntity()
    {
        self::$lastEntity = false;
    }

    public static function afterEntityUpdate(Event $event)
    {
        $id = $event->getParameters()['id'];
        static::updateReferenceData($id);
    }

    public static function afterEntityAdd(Event $event)
    {
        $id = $event->getParameters()['id'];
        static::updateReferenceData($id);
    }

    protected static function updateReferenceData($id)
    {

        $entity = static::getLastEntity();
        static::resetLastEntity();

        if (!$entity) {
            return false;
        }

        if (!$id) {
            return false;
        }

        $entity->setId($id);

        $refHash = spl_object_hash($entity);
        $refData = $entity->getData();
        self::$savedReference[$refHash] = true;
        $link = self::$registerReferences[$refHash];
        if (!$link) {
            return false;
        }
        /** @var HighLoadEntity $object */
        $object = $link['object'];
        $objectId = $object->getId();

        $objectHash = spl_object_hash($object);

        $fields = $link['fields'];
        $data = [];

        if (!$fields) {
            return false;
        }

        foreach ($fields as $objectKey => $refKey) {
            $data[$objectKey] = $refData[$refKey];
        }

        if (isset(self::$savedReference[$objectHash]) && $objectId) {
            $res = $object::update($objectId, $data);
        } else {
            foreach ($data as $key => $val) {
                $object->setField($key, $val);
            }
        }

        return true;
    }
}