<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock;


use Iswin\Borm\Common\EntityInterface;

/**
 * Интерфейс для High Load сущностей
 *
 * Interface HighLoadEntityInterface
 * @package Iswin\Borm\HlBlock
 */
interface HighLoadEntityInterface extends EntityInterface
{
    public static function getMap();

    public function setField($fieldName, $value);

    public function getField($fieldName);

    public function getXmlId();
}