<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\HlBlock;

use Bitrix\Main\Application;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Event;
use Bitrix\Main\Loader;
use Bitrix\Main\Entity;
use Bitrix\Highloadblock as HLI;
use Iswin\Borm\Bitrix\UserFieldEnumTable;
use Iswin\Borm\Common\EntityInterface;
use Iswin\Borm\Exceptions\ExceptionEntity;


Loader::includeModule('highloadblock');

/**
 * Базовый класс для любой сущности HighLoad блока.
 * ArrayAccess - Имплементация позволяет обращаться с элементами HighLoad блоков
 * как с объектами, так и как с массивами.
 * Iterator - Имплемантация позволяет совершать обработку объекта
 * как ассоциативного массива в цикле.
 *
 * Class HighLoadEntity
 * @package Iswin\Borm\HlBlock
 */
abstract class HighLoadEntity extends DataManager  implements \ArrayAccess, \Iterator, HighLoadEntityInterface
{

    /**
     * Максимальный размер пакета для поиска соответствий в БД
     *
     * @see HighLoadEntity::mergeWithDbByFilter()
     */
    const MAX_FILL_DB_PACK = 500;
    /**
     * регулярное выражение для удаления "table" из названия класса
     */
    const PREG_REMOVE_TABLE_FROM_CLASS = "#^(.+)Table$#Umsi";

    /**
     * Статус состояния: новый объект (еще не сохранен)
     */
    const STATUS_NEW = 1;
    /**
     * Статус состояния: загруженный объект
     */
    const STATUS_LOADED = 2;
    /**
     * Статус состояния:  объект сохранен
     */
    const STATUS_SAVED = 3;
    /**
     * Статус состояния: объект добавлен
     */
    const STATUS_ADDED = 4;
    /**
     * Статус состояния: объект обновлен
     */
    const STATUS_UPDATED = 5;
    /**
     * Статус состояния: объект слит с базой данных
     */
    const STATUS_MERGED = 6;
    /**
     * Статус состояния: объект сливался с БД, но изменений в нем не зафиксировано
     */
    const STATUS_SKIP_MERGED = 7;

    protected $statusDetail = self::STATUS_NEW;
    protected $status = self::STATUS_NEW;


    protected $data;
    /**
     * коды полей, которые запрещено обновлять для этой сущности
     *
     * @var array
     */
    protected $protectedUpdateFields = [];

    protected static $enums = array();


    public function __construct ($data)
    {
        $this->data = $data;
    }

    /**
     * Возвращает пустой инстанс сущности
     *
     * @param array $data
     * @return HighLoadEntity
     */
    public static function getInstance($data = [])
    {
        return new static($data);
    }

    /**
     * Дефолтный метод для слияния внешних объектов с БД ( @see HighLoadEntity::mergeWithDbByFilter )
     * В каждой фабрике может быть переопределен если отличается от текущего.
     *
     * @param $rows
     * @param $referenceMode - режим сохранения связей
     * @param $onlyUpdate - если true - то запрещено создавать новые объекты, только обновлять
     *
     * @see HlEntityFlushManager::REFERENCE_NOT
     * @see HlEntityFlushManager::REFERENCE_ONLY_SET_ID
     * @see HlEntityFlushManager::REFERENCE_NOT_BACK_REFERENCE
     * @see HlEntityFlushManager::REFERENCE_FULL_SAVE
     *
     * @return int
     */
    public static function mergeWithDb($rows, $referenceMode = 0, $onlyUpdate = false)
    {
        return self::mergeWithDbByFilter($rows, ['=ID' => 'ID'], $referenceMode, $onlyUpdate);
    }


    /**
     * Возвращает карту hl блока в виде ассоциативного массива
     *
     * @return Entity\Field[]
     */
    public static function getAssocMap()
    {
        $ret = [];

        $mapItems = static::getMap();
        /** @var Entity\Field $mapItem */
        foreach ($mapItems as $mapItem) {
            $ret[$mapItem->getName()] = $mapItem;
        }

        return $ret;
    }

    /**
     * Возвращает поля Highload блока
     *
     * @return array|false
     */
    public static function getHighLoadBlock() {
        $hlblock   = HLI\HighloadBlockTable::getList(
            [
                'filter' => ['TABLE_NAME' => static::getTableName()]
            ]
        )->fetch();
        return $hlblock;
    }

    public static function onAfterAdd (Entity\Event $event)
    {
        FlushManager::afterEntityAdd($event);
        parent::onAfterAdd($event);
    }

    public static function onAfterUpdate (Entity\Event $event)
    {
        parent::onAfterUpdate($event);
    }

    /**
     * Убирает мусор из $data который мог возникнуть из-за выборки связанных объектов
     *
     * @return $this
     */
    public function clearNoAssocSelectData()
    {
        $assocMap = static::getAssocMap();
        foreach ($this->data as $key => $value) {
            if (!isset($assocMap[$key])) {
                unset($this->data[$key]);
            }
        }
        return $this;
    }

    /**
     * Сохраняет объект, вместе с вложенными референсами
     * @see HlEntityFlushManager::flush()
     *
     * @param int $referenceMode - режим сохранения связей
     *
     * @see HlEntityFlushManager::REFERENCE_NOT
     * @see HlEntityFlushManager::REFERENCE_ONLY_SET_ID
     * @see HlEntityFlushManager::REFERENCE_NOT_BACK_REFERENCE
     * @see HlEntityFlushManager::REFERENCE_FULL_SAVE
     *
     * @return Entity\AddResult|Entity\UpdateResult|bool
     */
    public function save($referenceMode = 0)
    {
        if (!$this->beforeSave()) {
            return false;
        }

        $this->setStatusSaved();

        $result = FlushManager::getInstance($this)->flush($referenceMode);
        FlushManager::resetRegisterReferences();
        $this->afterSave();
        return $result;
    }

    /**
     * Удаляет сущность из базы
     *
     * @return Entity\DeleteResult
     */
    public function remove()
    {
        $id = $this->getId();
        /** Так битрикс к названию объекта добавляет неймспейc, то события отловить тяжело, исправляем: */
        $classNameWithNs = get_called_class();
        $className = array_pop(explode('\\', $classNameWithNs));
        $className = preg_replace(self::PREG_REMOVE_TABLE_FROM_CLASS, '$1', $className);

        $eventName = $className . DataManager::EVENT_ON_BEFORE_DELETE;

        $event = new Event('', $eventName, ['id' => ['ID' => $id]]);
        $event->send();

        $data = $this->getData();
        $res = static::delete($this->getId());

        $eventName = $className . DataManager::EVENT_ON_AFTER_DELETE;
        $event = new Event('', $eventName, ['id' => ['ID' => $id]]);
        $event->send();

        return $res;
    }

    /**
     * Удаляет записи без предварительной их выборки, и (!!!!) без вызова события удаления
     *
     * @param $where
     * @return bool
     */
    public static function removeWithoutSelect($where)
    {
        if (!$where) {
            return false;
        }

        $wheres = [];
        foreach ($where as $key => $value) {
            if (!is_int($value)) {
                $value = "'{$value}'";
            }

            $wheres[] = "`{$key}` = {$value}";
        }

        $tableName = static::getTableName();
        if (!$tableName) {
            return false;
        }

        $sql = "DELETE FROM {$tableName} WHERE " . implode(" AND ", $wheres);
        Application::getConnection()->query($sql);
    }

    /**
     * методы для модификации данных перед сохранением (запускается только при вызове save())
     */
    public function beforeSave()
    {
        $assocMap = static::getAssocMap();
        foreach ($this->getData() as $key => $val) {
            $field = $assocMap[$key];
            if ($field instanceof Entity\BooleanField) {
                $this->setField($key, (bool)$val);
            }
        }

        return true;
    }

    /**
     * методы для модификации данных после сохранения (запускается только при вызове save())
     */
    public function afterSave()
    {
        return;
    }

    /**
     * Имплементация метода ArrayAccess::offsetSet()
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    /**
     * Возвращает все поля элемента HighLoad блока, в виде ассоциативного массива
     *
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @see EntityInterface::setData()
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Возвращает значения enum поля по его коду
     *
     * @param string $key - код поля
     * @return array
     *
     * <code>
     * $return = array(
     *   'value_id'      => 'value_name'
     * );
     * </code>
     */
    public static function getEnumValues($key) {

        $tableName = static::getTableName();

        if (!isset(self::$enums[$tableName])) {

            $hlBlock = static::getHighLoadBlock();

            if (!$hlBlock) {
                self::$enums[$tableName] = [];
                return [];
            }
            self::$enums[$tableName]['_object'] = "HLBLOCK_{$hlBlock['ID']}";
        }

        if (isset(self::$enums[$tableName][$key])) {
            return self::$enums[$tableName][$key]['values'];
        }

        $object = self::$enums[$tableName]['_object'];
        if (!$object) {
            return [];
        }

        self::$enums[$tableName][$key]['values'] = [];
        $xmlValues = [];
        $values = [];

        $valuesObj = UserFieldEnumTable::query()
            ->addSelect('*')
            ->addSelect('USER_FIELD.ENTITY_ID', 'ENTITY_ID')
            ->addSelect('USER_FIELD.FIELD_NAME', 'FIELD_NAME')
            ->addSelect('USER_FIELD.ID', 'FIELD_ID')
            ->addOrder('SORT', 'ASC')
            ->addFilter('=ENTITY_ID', $object)
            ->addFilter('=FIELD_NAME', $key)
            ->exec();

        while ($value = $valuesObj->fetch()) {
            $values[$value['ID']] = $value['VALUE'];
            $xmlValues[$value['ID']] = $value['XML_ID'];
        }

        self::$enums[$tableName][$key]['xml_values'] = $xmlValues;

        return self::$enums[$tableName][$key]['values'] = $values;
    }

    /**
     * Возвращает ассоциативный массив значений enum поля по его коду
     *
     * @param $key - код поля
     * @return array
     *
     * <code>
     * $return = array(
     *   'value_id'      => 'value_xml_id'
     * );
     * </code>
     */
    public static function getEnumXmlValues($key) {
        $tableName = static::getTableName();
        if (isset(self::$enums[$tableName][$key])) {
            return self::$enums[$tableName][$key]['xml_values'];
        }
        static::getEnumValues($key);
        return self::$enums[$tableName][$key]['xml_values'];
    }

    /**
     * Сливает коллекции объектов с бд по фильтру
     *
     * @param HighLoadEntity[] $allRows - коллекция
     * @param array $filterMap - фильтр для поиска соответствий.
     *
     * Где ключи условия выборки, а значение коды полей из коллеккции. Если поле не задано в коллекциях, то в качестве значения берется сам код поля.
     * Важно отметить, что для всех полей которые в $filterMap - должны быть установлены значения в объектах коллекции
     * Пример $filterMap:
     *
     * <code>
     *      $filterMap = [
     *          '=UF_XML_ID' => 'UF_XML_ID',
     *          '=UF_ACTIVE' => 'UF_ACTIVE'
     *      ];
     * </code>
     *
     * @param int $referenceMode - режим сохранения связей
     * @param bool $onlyUpdate - если true запрещено создавать новые объекты
     *
     * @see HlEntityFlushManager::REFERENCE_NOT
     * @see HlEntityFlushManager::REFERENCE_ONLY_SET_ID
     * @see HlEntityFlushManager::REFERENCE_NOT_BACK_REFERENCE
     * @see HlEntityFlushManager::REFERENCE_FULL_SAVE
     *
     * @return int
     */
    protected static function mergeWithDbByFilter($allRows, $filterMap, $referenceMode = 0, $onlyUpdate = false)
    {

        if (!$allRows || !$filterMap) {
            return false;
        }

        $newCount = 0;

        $steps = array_chunk($allRows, static::MAX_FILL_DB_PACK);
        foreach ($steps as $stepRows) {
            $newCount += static::mergeWithDbByFilterStep($stepRows, $filterMap, $referenceMode, $onlyUpdate);
        }

        return $newCount;
    }

    /**
     * Запускается перед началом выборки имеющехся данных перед мержем
     *
     * @param $select
     * @param $filter
     * @param $filterMap
     */
    public static function onBeforeMergeSelect(&$select, &$filter, $filterMap)
    {
        return;
    }

    /**
     * Запускается после мержа объекта
     *
     * @param HighLoadEntity $oldEntity - объект в БД
     * @param HighLoadEntity $newEntity - новый объект
     * @param array $mergedFields - список полей которые были обновлены
     */
    public static function onMerge(HighLoadEntityInterface $oldEntity, HighLoadEntityInterface $newEntity, $mergedFields = [])
    {
        return;
    }

    /**
     * @param $rows
     * @param $filterMap
     * @param int $referenceMode
     * @param bool $onlyUpdate
     * @return int
     * @throws ExceptionEntity
     */
    protected static function mergeWithDbByFilterStep($rows, $filterMap, $referenceMode = 0, $onlyUpdate = false)
    {

        $entityMap = [];

        $filter = [];
        $filterReverse = array_flip($filterMap);

        $select = [];


        $newCount = 0;

        /** @var HighLoadEntity $row */
        foreach ($rows as $row) {
            $data = $row->getData();

            if (!$select) {
                foreach ($data as $key => $val) {
                    $select[] = $key;
                }
            }

            $values = [];

            foreach ($filterReverse as $key => $filterKey) {
                if (!isset($data[$key])) {
                    $objectPrint = print_r($row->getData(), true);
                    $message = "{$key} не задан для объекта \n======\n {$objectPrint} \n======\n но присутсвует в фильтре.";
                    throw new ExceptionEntity($message);
                }

                if (!isset($filter[$filterKey])) {
                    $filter[$filterKey] = [];
                }
                $filter[$filterKey][] = $data[$key];
                if (is_array($data[$key])) {
                    $message = "Запрещено использовать поля со значениями в виде массива при слиянии объектов с базой данных";
                    $e = new ExceptionEntity($message);
                    $data = $filterMap;
                    $data['object'] = get_class($row);
                    throw $e->setData($data);
                }
                $values[$key] = (string)$data[$key];
            }


            ksort($values);
            $uniqueKey = md5(serialize($values));
            $entityMap[$uniqueKey] = $row;
        }

        if (!in_array('ID', $select)) {
            $select[] = 'ID';
        }

        static::onBeforeMergeSelect($select, $filter, $filterMap);

        $existsRows = static::query()
            ->setSelect($select)
            ->setFilter($filter)
            ->exec();


        /** @var HighLoadEntity $row */
        while ($row = $existsRows->fetch()) {

            $values = [];
            foreach ($row->getData() as $key => $val) {
                if (isset($filterReverse[$key])) {
                    $values[$key] = (string)$val;
                }
            }

            ksort($values);
            $uniqueKey = md5(serialize($values));

            if (!isset($entityMap[$uniqueKey])) {
                continue;
            }
            /** @var HighLoadEntity $entity */
            $entity = $entityMap[$uniqueKey];
            $protectedFields = $entity->getProtectedUpdateFields();

            $needRowSave = false;
            $mergedFields = [];

            $entityData = $entity->getData();
            foreach ($entityData as $key => $val) {
                if ($key == 'ID') {
                    continue;
                }
                /**
                 *  по сути это защита, если значение не установлено, чтоб не затирать старое
                 *  @todo подумать правильно ли?
                 */
                if (!$val) {
                    continue;
                }

                if (in_array($key, $protectedFields)) {
                    continue;
                }

                $oldVal = $row->getField($key);

                if (static::isNeedSave($oldVal, $val)) {
                    $mergedFields[] = $key;
                    $row->setField($key, $val);
                    $needRowSave = true;
                }
            }


            $entity->setStatusSkipMerged();

            if ($needRowSave) {
                $res = $row->save($referenceMode);

                if (!$res->isSuccess()) {
                    $message = "Ошибка сохранения объекта: " . implode("; ", $res->getErrorMessages());
                    $data = $row->getData();
                    $data['object'] = get_class($row);
                    $e = new ExceptionEntity($message);
                    throw $e->setData($data);
                }

                $entity->setStatusMerged();
            }

            $entity->setId($row->getId());
            static::onMerge($row, $entity, $mergedFields);
            unset($entityMap[$uniqueKey]);
        }

        if (!$onlyUpdate) {
            /** @var HighLoadEntity $entity */
            foreach ($entityMap as $entity) {
                $entity->save($referenceMode);
                $newCount++;
            }
        }

        return $newCount;
    }

    /**
     * @param $oldVal
     * @param $newVal
     * @return bool
     */
    protected static function isNeedSave($oldVal, $newVal)
    {
        if (is_array($newVal)) {
            sort($oldVal);
            sort($val);

            if (current($newVal) instanceof EntityInterface) {
                /** МНожественные поля со значением типа объекта не мержатся */
                return false;
            } else {
                return $oldVal != $val;
            }

        } else {
            if ($newVal instanceof EntityInterface) {
                if (!($oldVal instanceof EntityInterface)) {
                    return true;
                }

                return $newVal->getId() != $oldVal->getId();
            }

            return $oldVal != $newVal;
        }
    }

    /**
     * Имплементация метода ArrayAccess::offsetExists()
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     *
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    /**
     * Имплементация метода ArrayAccess::offsetUnset()
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset
     * @return bool
     */
    public function offsetUnset($offset) {
        $isset = isset($this->data[$offset]);
        unset($this->data[$offset]);
        return $isset;
    }

    /**
     * Имплементация метода ArrayAccess::offsetGet()
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     *
     * @param mixed $offset
     * @return null
     */
    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    /**
     * Магический метод, для валидной отработки функции isset()
     *
     * @param $name
     * @return bool
     */
    public function __isset ($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * Возвращает ID  элемента HighLoad блока
     *
     * @return integer
     */
    public function getId() {
        return $this->getField('ID');
    }

    /**
     * Возвращает внешний код объекта
     *
     * @return int
     */
    public function getXmlId()
    {
        return $this->getField('UF_XML_ID') ? : $this->getId();
    }

    /**
     * Устанавливает ID  элемента HighLoad блока
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->setField('ID', $id);
        return $this;
    }

    /**
     * Возвращает значение поля по его коду
     *
     * @param $key - код поля
     * @return mixed
     */
    public function getField($key) {
        return $this->data[$key];
    }

    /**
     * Устанавливае значение поля по его коду
     *
     * @param $key - код поля
     * @return $this
     */
    public function setField($key, $val) {
        $this->data[$key] = $val;
        return $this;
    }

    /**
     * Добавляет код поля, которое запрещено будет обновлять при мерже с БД
     *
     * @param $fieldCode
     * @return $this
     */
    protected function addProtectedUpdateField($fieldCode)
    {
        $this->protectedUpdateFields[] = $fieldCode;
        return $this;
    }

    /**
     * Возвращает список полей, которые запрещено обновлять при мерже с БД
     *
     * @return array
     */
    public function getProtectedUpdateFields()
    {
        return $this->protectedUpdateFields;
    }


    /**
     * Имплементация метода Iterator::rewind()
     * @link http://php.net/manual/en/iterator.rewind.php
     */
    public function rewind() {
        reset($this->data);
    }

    /**
     * Имплементация метода Iterator::current()
     * @link http://php.net/manual/en/iterator.current.php
     */
    public function current() {
        return current($this->data);
    }

    /**
     * Имплементация метода Iterator::key()
     * @link http://php.net/manual/en/iterator.current.php
     */
    public function key() {
        return key($this->data);
    }

    /**
     * Имплементация метода Iterator::next()
     * @link http://php.net/manual/en/iterator.next.php
     */
    public function next() {
        next($this->data);
    }

    /**
     * Имплементация метода Iterator::valid()
     * @link http://php.net/manual/en/iterator.valid.php
     */
    public function valid() {
        return key($this->data) !== null;
    }

    /**
     * @return Query
     */
    public static function query ()
    {
        return parent::query();
    }

    /**
     * Возвращает глобальный статус статус состояния объекта
     * @see HighLoadEntity::STATUS_NEW
     * @see HighLoadEntity::STATUS_LOADED
     * @see HighLoadEntity::STATUS_SAVED
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }


    /**
     * Возвращает детальный статус
     *
     * @see HighLoadEntity::STATUS_ADDED
     * @see HighLoadEntity::STATUS_UPDATED
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return int
     */
    public function getStatusDetail()
    {
        return $this->statusDetail;
    }

    /**
     * Проверяет статус
     * @see HighLoadEntity::STATUS_NEW
     *
     * @return bool
     */
    public function isStatusNew()
    {
        return $this->getStatus() == self::STATUS_NEW;
    }

    /**
     * Проверяет статус
     * @see HighLoadEntity::STATUS_LOADED
     *
     * @return bool
     */
    public function isStatusLoaded()
    {
        return $this->getStatus() == self::STATUS_LOADED;
    }


    /**
     * Проверяет статус
     * @see HighLoadEntity::STATUS_SAVED
     *
     * @return bool
     */
    public function isStatusSaved()
    {
        return $this->getStatus() == self::STATUS_SAVED;
    }


    /**
     * Проверяет статус
     * @see HighLoadEntity::STATUS_ADDED
     *
     * @return bool
     */
    public function isStatusAdded()
    {
        return $this->getStatusDetail() == self::STATUS_ADDED;
    }


    /**
     * Проверяет статус
     * @see HighLoadEntity::STATUS_UPDATED
     *
     * @return bool
     */
    public function isStatusUpdated()
    {
        return $this->getStatusDetail() == self::STATUS_UPDATED;
    }

    /**
     * Проверяет статус
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return bool
     */
    public function isStatusMerged()
    {
        return $this->getStatusDetail() == self::STATUS_MERGED;
    }

    /**
     * Проверяет статус
     * @see HighLoadEntity::STATUS_SKIP_MERGED
     *
     * @return bool
     */
    public function isStatusSkipMerged()
    {
        return $this->getStatusDetail() == self::STATUS_SKIP_MERGED;
    }

    /**
     * Устанавливает статус
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return $this
     */
    public function setStatusMerged()
    {
        $this->statusDetail = self::STATUS_MERGED;
        return $this;
    }

    /**
     * Устанавливает статус
     * @see HighLoadEntity::STATUS_SKIP_MERGED
     *
     * @return $this
     */
    public function setStatusSkipMerged()
    {
        $this->statusDetail = self::STATUS_SKIP_MERGED;
        return $this;
    }

    /**
     * Устанавливает статус
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return $this
     */
    public function setStatusUpdated()
    {
        $this->statusDetail = self::STATUS_UPDATED;
        return $this;
    }

    /**
     * Устанавливает статус
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return $this
     */
    public function setStatusAdded()
    {
        $this->statusDetail = self::STATUS_ADDED;
        return $this;
    }

    /**
     * Устанавливает статус
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return $this
     */
    public function setStatusSaved()
    {
        $this->status = self::STATUS_SAVED;
        return $this;
    }

    /**
     * Устанавливает статус
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return $this
     */
    public function setStatusLoaded()
    {
        $this->status = self::STATUS_LOADED;
        return $this;
    }

    /**
     * Устанавливает статус
     * @see HighLoadEntity::STATUS_MERGED
     *
     * @return $this
     */
    public function setStatusNew()
    {
        $this->status = self::STATUS_SKIP_MERGED;
        return $this;
    }

    protected static function validateIds(&$ids)
    {
        foreach ($ids as $index => $id) {
            $id = (int)trim($id);
            if (!$id) {
                unset($ids[$index]);
            }
        }
    }

    /**
     * Возвращает массив объектов по их ID
     *
     * @param $ids
     * @param $select
     * @param $order
     * @return HighLoadEntity[]|array
     */
    public static function getByIds($ids, $select = [], $order = [])
    {
        static::validateIds($ids);
        if (!$ids) {
            return [];
        }

        if (!$select) {
            $select = ['*'];
        }

        if (!$order) {
            $order = ['ID' => '*'];
        }

        $rows = static::query()
            ->setFilter(['=ID' => $ids])
            ->setSelect($select)
            ->setOrder($order)
            ->exec()
            ->fetchAll();
        return $rows;
    }
}