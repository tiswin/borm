<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Common;

/**
 * Интерфейс всех сущеностей ORM
 *
 * Interface EntityInterface
 * @package Iswin\Borm\Common
 */
interface EntityInterface
{
    /**
     * Сохраняет объект
     *
     * @return mixed
     */
    public function save();

    /**
     * Возвращает уникальный идентификатор объекта
     *
     * @return mixed
     */
    public function getId();

    /**
     * Устанавливает уникальный идентификатор объекта
     *
     * @param $id
     * @return mixed
     */
    public function setId($id);

    /**
     * Возвращает пустой инстанс сущности
     *
     * @param array $data
     * @return EntityInterface
     */
    public static function getInstance($data = []);

    /**
     * Возвращает все данные объекта, выбранные из базы данных
     *
     * @return mixed
     */
    public function getData();


    /**
     * Устанавливает все данные объекта
     *
     * @param array $data
     * @return $this
     */
    public function setData($data);

    /**
     * Возвращает название объекта
     *
     * @return string
     */
    public function getName();

    /**
     * Возвращает объект для пстроения запроса выборки
     *
     * @return QueryInterface
     */
    public static function query();

    /**
     * Возвращает коллекцию объектов по массиву их ID
     *
     * @param $ids
     * @return mixed
     */
    public static function getByIds($ids, $select = [], $order = []);

    /**
     * Возвращает объект по его ID
     *
     * @param $id
     * @return EntityInterface
     */
    public static function getById($id);

    public function getField($propCode);

    public function setField($propCode, $value);
}