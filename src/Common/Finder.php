<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Common;
use Iswin\Borm\HlBlock\HighLoadEntity;
use Iswin\Borm\Iblock\FinderResult;
use Iswin\Borm\Iblock\IblockEntity;


/**
 * Базовый класс для поиска репозиториев
 *
 * Class Finder
 * @package Iswin\Borm\Common
 */
abstract class Finder
{
    /**
     * регулярное выражение для выдергивания названия класса
     */
    const PREG_CLASS_NAME = "#.*class\s+([0-9A-Z_]+)(\s|$)#Umsi";
    /**
     * регулярное выражение для выдергивания пространства имен класса
     */
    const PREG_NAMESPACE = "#.*namespace\s+([0-9A-Z_\\\]+)(\s|;)#Umsi";


    /**
     * путь к локальным модулям bitrxi
     */
    const LOCAL_MODULE_PATH = '/local/modules/';
    /**
     * путь к глобальным битрикс модулям
     */
    const BITRIX_MODULE_PATH = '/bitrix/modules/';


    protected $enableBitrixModule = false;
    protected $result = false;


    protected static $cache = [];

    protected function __construct ()
    {
    }

    /**
     * Возвращает инстанс поиска
     *
     * @return static
     */
    public static function getInstance()
    {
        return new static();
    }


    /**
     * Включает поиск по битрикс модулям
     *
     * @return $this
     */
    public function enableBitrixModule()
    {
        $this->enableBitrixModule = true;
        return $this;
    }

    /**
     * Выключает поиск по битрикс модулям
     *
     * @return $this
     */
    public function disableBitrixModule()
    {
        $this->enableBitrixModule = false;
        return $this;
    }


    /**
     * Возвращает репозитории инфоблока по его коду
     *
     * @param $code - код регистронезависимый
     * @return FinderResult|HighLoadEntity|bool
     */
    public function search($code)
    {
        $cacheKey = "{$this->enableBitrixModule}|$code";
        if (isset(self::$cache[$cacheKey])) {
            return self::$cache[$cacheKey];
        }

        $this->result = false;
        $this->searchInModules(static::LOCAL_MODULE_PATH, $code);

        if (!$this->enableBitrixModule) {
            return self::$cache[$cacheKey] = $this->result;
        }

        $this->searchInModules(static::BITRIX_MODULE_PATH, $code);
        return self::$cache[$cacheKey]  = $this->result;
    }



    /**
     * @param $dir
     * @param $code
     */
    protected function searchInModules($dir, $code)
    {

        $dir = $_SERVER['DOCUMENT_ROOT'] . "{$dir}";

        if (!file_exists($dir)) {
            return;
        }

        $iterator = new \DirectoryIterator($dir);
        /** @var \DirectoryIterator $dir */
        foreach ($iterator as $file) {
            if ($file->isDot()) {
                continue;
            }

            $modulePath = $file->getPathName();
            $this->searchInModule($modulePath, $code);
        }
    }

    /**
     * @param $path
     * @param $iblockCode
     */
    protected function recursiveSearchInPath($path, $code)
    {
        $iterator = new \DirectoryIterator($path);
        /** @var \DirectoryIterator $dir */
        foreach ($iterator as $file) {
            if ($file->isDot()) {
                continue;
            }

            $filePath = $file->getPathname();

            if ($file->isDir()) {
                $this->recursiveSearchInPath($filePath, $code);
                continue;
            }

            $filePath = $file->getPathname();


            $content = file_get_contents($filePath);

            if (!preg_match(self::PREG_CLASS_NAME, $content, $matches)) {
                continue;
            }

            $className = $matches[1];

            if (!preg_match(self::PREG_NAMESPACE, $content, $matches)) {
                continue;
            }

            $nameSpace = $matches[1];

            /** @var IblockEntity $className */
            $className = "{$nameSpace}\\{$className}";
            $this->checkResult($className, $code);
        }
    }

    abstract protected function searchInModule($modulePath, $code);
    abstract protected function checkResult($className, $code);

}