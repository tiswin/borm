<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Common;


/**
 * класс обертка для любых общих массивов
 *
 * Обращаться с ним также как и с обычным массивом:
 *
 * <code>
 *   $ar = [1 => 2, 3 => 4];
 *   $collection = Collection::getInstance($ar);
 *   var_dump($collection[1]); //выведет 2
 * </code>
 *
 * Class Collection
 * @package Iswin\Borm\Common
 */
class Collection implements \ArrayAccess, \Iterator
{
    protected $data;

    protected function __construct ($data)
    {
        $this->data = $data;
    }

    public static function getInstance($data)
    {
        return new Collection($data);
    }

    /**
     * Имплементация метода ArrayAccess::offsetExists()
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     *
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    /**
     * Имплементация метода ArrayAccess::offsetUnset()
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     *
     * @param mixed $offset
     * @return bool
     */
    public function offsetUnset($offset) {
        $isset = isset($this->data[$offset]);
        unset($this->data[$offset]);
        return $isset;
    }

    /**
     * Имплементация метода ArrayAccess::offsetGet()
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     *
     * @param mixed $offset
     * @return null
     */
    public function offsetGet($offset) {
        return isset($this->data[$offset]) ? $this->data[$offset] : null;
    }

    /**
     * Магический метод, для валидной отработки функции isset()
     *
     * @param $name
     * @return bool
     */
    public function __isset ($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * Имплементация метода ArrayAccess::offsetSet()
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     *
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->data[] = $value;
        } else {
            $this->data[$offset] = $value;
        }
    }

    /**
     * Возвращает все поля элемента HighLoad блока, в виде ассоциативного массива
     *
     * @return array
     */
    public function getData() {
        return $this->data;
    }


    /**
     * Имплементация метода Iterator::rewind()
     * @link http://php.net/manual/en/iterator.rewind.php
     */
    public function rewind() {
        reset($this->data);
    }

    /**
     * Имплементация метода Iterator::current()
     * @link http://php.net/manual/en/iterator.current.php
     */
    public function current() {
        return current($this->data);
    }

    /**
     * Имплементация метода Iterator::key()
     * @link http://php.net/manual/en/iterator.current.php
     */
    public function key() {
        return key($this->data);
    }

    /**
     * Имплементация метода Iterator::next()
     * @link http://php.net/manual/en/iterator.next.php
     */
    public function next() {
        next($this->data);
    }

    /**
     * Имплементация метода Iterator::valid()
     * @link http://php.net/manual/en/iterator.valid.php
     */
    public function valid() {
        return key($this->data) !== null;
    }

}