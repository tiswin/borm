<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Common;


/**
 * Интерфейс запросов к hl и инфо- блокам
 *
 * Interface QueryInterface
 * @package Iswin\Borm\Common
 */
interface QueryInterface
{

    /**
     * Добавляет поле для выборки
     *
     * @param $code
     * @param $alias
     * @return $this
     */
    public function addSelect($code, $alias = '');

    /**
     * Возвращает все поля выборки
     *
     * @return $this
     */
    public function getSelect();

    /**
     * Добавляет значение для фильтрации
     *
     * @param $key
     * @param $value
     * @return $this
     */
    public function addFilter($key, $value);

    /**
     * Возвращает результат выборки
     *
     * @return EntityInterface[]
     */
    public function fetchAll();

    /**
     * Возвращает пустой объект для которой осуществляется выборка
     * Используется для идентификации репозитория выборки
     *
     * @return EntityInterface
     */
    public function getEntityObject();

    /**
     * @param $order
     * @return QueryInterface
     */
    public function setOrder($order);

    public function getOrder();
}