<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;

use Iswin\Borm\Iblock\Element\ElementEntity;
use Iswin\Borm\Iblock\Element\ElementEntityInterface;
use Iswin\Borm\Iblock\Section\SectionEntity;
use Iswin\Borm\Iblock\Section\SectionEntityInterface;


/**
 * Коллекция сущностей инфоблока
 *
 * Class FinderResult
 * @package Iswin\Borm\Iblock
 */
class FinderResult
{
    /**
     * @var SectionEntity
     */
    protected $sectionRepo;
    /**
     * @var ElementEntity
     */
    protected $itemsRepo;

    protected function __construct ()
    {
    }

    /**
     * @return FinderResult
     */
    public static function getInstance()
    {
        return new static();
    }

    /**
     * @return SectionEntity
     */
    public function getSectionEntity ()
    {
        return $this->sectionRepo;
    }

    /**
     * @param SectionEntityInterface $sectionRepo
     * @return $this
     */
    public function setSectionEntity (SectionEntityInterface $sectionRepo)
    {
        $this->sectionRepo = $sectionRepo;
        return $this;
    }

    /**
     * @return ElementEntity
     */
    public function getElementEntity ()
    {
        return $this->itemsRepo;
    }

    /**
     * @param ElementEntityInterface $itemsRepo
     * @return $this
     */
    public function setElementEntity (ElementEntityInterface $itemsRepo)
    {
        $this->itemsRepo = $itemsRepo;
        return $this;
    }
}