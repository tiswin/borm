<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;

use Iswin\Borm\Common\QueryInterface;
use Iswin\Borm\Iblock\Repository\SelectResultInterface;

/**
 * Класс для осуществления запросов выборки из инфоблоков
 *
 * Class Query
 * @package Iswin\Borm\Iblock
 */
abstract class Query implements IblockQueryInterface
{


    /**
     * Добавляет в выборку свойство по его коду
     * должен быть переопределен в дочерних классах
     *
     * @param $propCode
     * @return $this
     */
    abstract public function addSelectProp($propCode);

    protected $linkedManagers =[];

    protected $select = [];
    protected $filter = [];
    protected $order = [];

    protected $cacheTag;
    protected $cnt;
    protected $indexArray;
    /**
     * По умолчанию кеш отключаем, о включении кеша надо заботиться при выборках
     * @var int
     */
    protected $cacheTime = 1;
    /** @var  PageNavInterface */
    protected $nav;
    protected $group;

    protected $lastSelectedCount = 0;
    protected $mapSelectedCount = false;

    /**
     * @var IblockEntity
     */
    protected $entity;

    /**
     * Query constructor.
     * @param IblockEntity $entityBase
     */
    protected function __construct (IblockEntityInterface $entityBase)
    {
        $this->entity = $entityBase;
        $this->addFilter('=IBLOCK_ID', $entityBase->getIblockId());
    }

    /**
     * Возвращает instance
     *
     * @param IblockEntityInterface $entity
     * @return Query
     */
    public static function getInstance(IblockEntityInterface $entity)
    {
        return new static($entity);
    }

    /**
     * @return IblockEntity
     */
    protected function getEntity()
    {
        return $this->entity;
    }

    /**
     * @see QueryInterface
     *
     * @return IblockEntity
     */
    public function getEntityObject ()
    {
        return $this->getEntity();
    }

    /**
     * Возвращает массив полей для выборки
     *
     * @return array
     */
    public function getSelect ()
    {
        return $this->select;
    }

    /**
     * Устанавливает массив полей для выбокри
     *
     * @param $select
     * @return $this
     */
    public function setSelect ($select)
    {
        $this->select = $select;
        return $this;
    }

    /**
     * Добавляет поле для выборки
     *
     * @param $key
     * @param $alias - игнорируется в этом класса
     * @return $this
     */
    public function addSelect($key, $alias = null)
    {
        $this->select[] = $key;
        return $this;
    }


    /**
     * Возвращает фильтр выборки
     *
     * @return array
     */
    public function getFilter ()
    {
        return $this->filter;
    }

    /**
     * Устанавливает фильтр выборки
     *
     * @param $filter
     * @return $this
     */
    public function setFilter ($filter)
    {
        $this->filter = $filter;
        return $this;
    }

    /**
     * Добавляет условие фильтрации
     *
     * @param $key
     * @param $val
     * @return $this
     */
    public function addFilter($key, $val)
    {
        $this->filter[$key] = $val;
        return $this;
    }

    /**
     * Возвращает условие для сортировки
     *
     * @return array
     */
    public function getOrder ()
    {
        return $this->order;
    }

    /**
     * Устанавливает условие для сортировки
     *
     * @param $order
     * @return $this
     */
    public function setOrder ($order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * Возвращает массив тегов для кеша
     *
     * @return mixed
     */
    public function getCacheTag ()
    {
        return $this->cacheTag;
    }

    /**
     * Добавляет тег для кеша
     *
     * @param $cacheTag
     * @return $this
     */
    public function addCacheTag ($cacheTag)
    {
        $this->cacheTag = $cacheTag;
        return $this;
    }

    /**
     * Возвращает время кеширования
     *
     * @return mixed
     */
    public function getCacheTime ()
    {
        return $this->cacheTime;
    }

    /**
     * Устанавливает время кеширования запроса
     *
     * @param $cacheTime
     * @return $this
     */
    public function setCacheTime ($cacheTime)
    {
        $this->cacheTime = $cacheTime;
        return $this;
    }

    /**
     * Возвращает параметры постраничной навигации
     *
     * @return PageNavInterface|null
     */
    public function getNav ()
    {
        return $this->nav;
    }

    /**
     * Устанавливает параметры постраничной навигации
     *
     * @param PageNavInterface $nav
     * @return $this
     */
    public function setNav (PageNavInterface $nav)
    {
        $this->nav = $nav;
        return $this;
    }

    /**
     * Возвращает один результат
     *
     * @return IblockEntity
     */
    public function getOneResult()
    {
        return current($this->fetchAll());
    }

    /**
     * Возвращает все результаты
     * @return IblockEntity[]
     */
    public function fetchAll()
    {
        $select = $this->getSelect();
        if ($select && !in_array('ID', $select)) {
            $this->addSelect('ID');
        }

        if ($select && !in_array('IBLOCK_ID', $select)) {
            $this->addSelect('IBLOCK_ID');
        }
        /** @var IblockEntity $entity */
        $entity = $this->getEntity();
        /** @var SelectResultInterface $result */
        $result = $entity::getRepository()->getList($this);
        $this->lastSelectedCount = $result->getCount();
        $ret = [];
        foreach ($result->getItems() as $item) {
            $ret[] = $this->getEntity()::getInstance($item);
        }
        if ($nav = $this->getNav()) {
            $nav->setAllCount($result->getCount());
        }
        return $ret;
    }



    /**
     * Добавляет в выборку свойство - связь с элементами, разделами, или справочниками
     * при добавлении в select такого свойства, будет выполнен один дополнительный запрос
     * на всю выборку, в результате в качестве значения таких свойств будут возвращены объекты
     *
     * Пример выбора товара со связанным через свойство BRAND брендом:
     * <code>
     * $row = Product::query()
     *   ->addSelect('ID')
     *   ->addSelectLink(
     *       'BRAND',
     *       Brand::query()
     *           ->addSelect('ID')
     *           ->addSelect('NAME')
     *           ->addSelect('CODE')
     *   )
     *   ->addFilter('=ID', $productId)
     *   ->getOneResult();
     * </code>
     *
     * @param string $propCode - код свойства
     * @param QueryInterface $query - объект запроса к связанным объектам (не должен включать фильтр)
     * @return $this
     */
    public function addSelectLink($propCode, QueryInterface $query)
    {
        $this->addSelectProp($propCode);
        $this->linkedManagers[] = LinkManager::getInstance($query, $propCode);
        return $this;
    }
}