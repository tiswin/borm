<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;


use Iswin\Borm\Common\QueryInterface;

interface IblockQueryInterface extends QueryInterface
{
    /**
     * @return PageNavInterface
     */
    public function getNav();

    public function setNav(PageNavInterface $nav);

    public function getFilter();

    public function setFilter($filter);

    public function getCacheTime();

    public function setCacheTime($time);

    public function getCacheTag();

    public function addCacheTag($tag);

    public function addSelectLink($propCode, QueryInterface $query);

    public function addSelectProp($propCode);
}