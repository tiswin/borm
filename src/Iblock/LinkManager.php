<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;

use Iswin\Borm\Common\EntityInterface;
use Iswin\Borm\Common\QueryInterface;
use Iswin\Borm\Exceptions\ExceptionEntity;
use Iswin\Borm\HlBlock\Query as HlQuery;
use Iswin\Borm\Iblock\Element\ElementQuery;
use Iswin\Borm\Iblock\Element\ElementQueryInterface;
use Iswin\Borm\Iblock\Section\SectionQuery;
use Iswin\Borm\Iblock\Section\SectionQueryInterface;


/**
 * Менеджер для выборки связанных свойств с элементом инфоблока:
 * - элементы инфоблока, разделы инфоблока, справочники
 *
 * Пример выбора товара со связанным через свойство BRAND брендом:
 * <code>
 * $row = Product::query()
 *   ->addSelect('ID')
 *   ->addSelectLink(
 *       'BRAND',
 *       Brand::query()
 *           ->addSelect('ID')
 *           ->addSelect('NAME')
 *           ->addSelect('CODE')
 *   )
 *   ->addFilter('=ID', $productId)
 *   ->getOneResult();
 * </code>
 *
 * Class LinkManager
 * @package Iswin\Borm\Iblock
 */
class LinkManager
{
    /**
     * Ключ для фильтрации связанных справочников
     */
    const DEFAULT_HL_FILTER = 'UF_XML_ID';

    /**
     * В случае если ID-шников связи будет передано много
     * То запрос для выборки связанных инфоблоков будет выполняться в несколько шагов.
     *
     * Эта константа определяет максимальное кол-во id для шага
     */
    const MAX_STEP_SIZE = 500;

    /**
     * @var QueryInterface
     */
    protected $query;
    protected $propCode;
    protected $linkIds = [];
    protected $links = [];

    protected function __construct (QueryInterface $query, $propCode)
    {
        $this->query = $query;
        $this->propCode = $propCode;
    }

    /**
     * @return QueryInterface
     */
    protected function getQuery()
    {
        return $this->query;
    }

    /**
     * Возвращает инстанс менеджера по запросу
     *
     * @param QueryInterface $query
     * @param string $propCode - код свойства
     * @return LinkManager
     */
    public static function getInstance(QueryInterface $query, $propCode)
    {
        return new static($query, $propCode);
    }

    /**
     * Возвращает код свойства в котором хранится связь
     *
     * @return mixed
     */
    public function getPropertyCode()
    {
        return $this->propCode;
    }

    /**
     * Добавляет ID (или XMl id - для справочников) связи
     * и ассоциирует ему объект элемента инфоблока, для простановки обратной ссылки
     *
     * @param $linkId
     * @param IblockEntity $element
     * @return $this
     */
    public function addLink($linkId, IblockEntityInterface $element)
    {
        if (!$linkId) {
            return $this;
        }

        $this->linkIds[] = $linkId;
        if (!isset($this->links[$linkId])) {
            $this->links[$linkId] = [];
        }

        $this->links[$linkId][] = $element;
        return $this;
    }

    /**
     * Выбирает все связанные объекты и проставляет ссылки на них
     * в соответсвующих объекта элементов инфоблока
     *
     * Если связи были проставлены возвращает true,
     * в противном случае false
     * @param $hlFilter - ключ для фильтрации связанных справочников
     * дело в том. что в случае элементов ИБ - это xml id, а в случае разделов - ID
     * @return bool
     */
    public function exec($hlFilter = false)
    {

        if (!$hlFilter) {
            $hlFilter = static::DEFAULT_HL_FILTER;
        }

        $ids = $this->linkIds;
        if (!$ids) {
            return false;
        }

        $steps = array_chunk($ids, static::MAX_STEP_SIZE);

        foreach ($steps as $stepIds) {
            $this->execStep($stepIds, $hlFilter);
        }

        return true;
    }

    protected function execStep($ids, $hlFilter)
    {
        foreach ($ids as $index => $id) {
            $id = trim($id);
            if (!$id) {
                unset($ids[$index]);
            }
        }

        if (!$ids) {
            return false;
        }

        $isHlLink = false;

        $query = $this->getQuery();
        if ($query instanceof HlQuery) {
            $query->addSelect($hlFilter);
            $query->addFilter('=' . $hlFilter, $ids);
            $isHlLink = true;
        } elseif ($query instanceof ElementQueryInterface || $query instanceof SectionQueryInterface) {
            $query->addFilter('=ID', $ids);
        } else {
            throw new ExceptionEntity("Неизвестный тип запроса для выбора связанных объектов: " . get_class($query));
        }

        $rows = $query->fetchAll();

        /** @var EntityInterface $row */
        foreach ($rows as $row) {
            $data = $row->getData();

            $linkId = $isHlLink ? $data[$hlFilter] : $data['ID'];
            $objects = $this->links[$linkId];
            if (!$objects) {
                continue;
            }
            /** @var IblockEntity $object */
            foreach ($objects as $object) {
                $object->addLinkedObject($this->getPropertyCode(), $row);
            }
        }

        return true;
    }

}