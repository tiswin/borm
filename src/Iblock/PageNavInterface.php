<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;

/**
 * Интерфейс для постраничной навигации
 *
 * Interface PageNavInterface
 * @package Iswin\Borm\Iblock
 */
interface PageNavInterface
{
    /**
     * Возвращает instance объекта
     *
     * @param int $page
     * @param int|null $pageSize
     * @return mixed
     */
    public static function getInstance($page = 1, $pageSize = null);

    /**
     * Устанавливает номер страницы
     *
     * @param $page
     * @return PageNavInterface
     */
    public function setPage($page);

    /**
     * Возвращает  номер страницы
     *
     * @return int
     */
    public function getPage();

    /**
     * Устанавливает размер страницы
     *
     * @param int $size
     * @return PageNavInterface
     */
    public function setPageSize($size);

    /**
     * Возвращает размер страницы (если null - значит постраничная навигация не используется)
     *
     * @return int|null
     */
    public function getPageSize();

    /**
     * Устанавливает общее кол-во записей подходящих под фильтр выбокри
     *
     * @param $count
     * @return PageNavInterface
     */
    public function setAllCount($count);

    /**
     * Возвращает общее кол-во записей подходящих под фильтр выборки
     *
     * @return int
     */
    public function getAllCount();

    /**
     * Возвращает количество страниц при постраничной навигации
     *
     * @return int
     */
    public function getPageCount();

    /**
     * Возвращает массив для подстановки в битриксовый GetList
     *
     * @return array
     */
    public function toArray();
}