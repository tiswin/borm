<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;


class PageNav implements PageNavInterface
{

    protected $page;
    protected $pageSize;
    protected $allCount;

    protected function __construct ($page = 1, $pageSize = null)
    {
        $this->setPage($page);
        $this->setPageSize($pageSize);
    }

    public static function getInstance ($page = 1, $pageSize = null)
    {
        return new static($page, $pageSize);
    }

    /**
     * @return mixed
     */
    public function getPage ()
    {
        return $this->page;
    }

    /**
     * @param $page
     * @return $this
     */
    public function setPage ($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPageSize ()
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     * @return $this
     */
    public function setPageSize ($pageSize)
    {
        $this->pageSize = $pageSize;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAllCount ()
    {
        return $this->allCount;
    }

    /**
     * @param $allCount
     * @return $this
     */
    public function setAllCount ($allCount)
    {
        $this->allCount = $allCount;
        return $this;
    }

    public function getPageCount ()
    {
        $pageSize = $this->getPageSize();
        $allCount = $this->getAllCount();

        if ($allCount == 0) {
            return 0;
        }

        if (!$pageSize) {
            return 1;
        }

        return ceil($allCount / $pageSize);
    }

    public function toArray ()
    {
        $ret = [
            'iNumPage' => $this->getPage()
        ];

        if ($pageSize = $this->getPageSize()) {
            $ret['nPageSize'] = $pageSize;
        }

        return $ret;
    }
}