<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Repository;

use Bitrix\Main\Application;
use \Bitrix\Main\Data\Cache;
use Bitrix\Main\Data\TaggedCache;
use Bitrix\Main\Loader;

Loader::includeModule('iblock');

/**
 * Базовый класс для репозиториев элементов и разделов инфоблока
 *
 * Class IblockRepository
 * @package Iswin\Borm\Iblock\Repository
 */
abstract class IblockRepository implements IblockRepositoryInterface
{

    const BASE_CACHE_DIR = 'borm';

    protected $iblock = [];
    protected $cacheInstance = false;

    protected $cacheStarted = false;

    protected function __construct($iblockCode, $cacheTime = 0)
    {
        $this->load($iblockCode, $cacheTime);
    }

    protected function isNeedResetCache()
    {
        return Application::getInstance()
            ->getContext()
            ->getRequest()
            ->get('clear_cache') == 'Y';
    }

    protected function isCacheStarted()
    {
        return $this->cacheStarted;
    }

    /**
     * Возвращает либо закешированные данные, либо false
     * и начинает кешировать
     *
     * @param $cacheTime
     * @param $cacheId
     * @return bool
     * @throws RepositoryException
     */
    protected function getCache($cacheTime, $cacheId)
    {
        $cache = $this->getCacheInstance();

        if ($this->isNeedResetCache()) {
            $cache->clean($cacheId, static::BASE_CACHE_DIR);
        }

        if ($this->isCacheStarted()) {
            throw new RepositoryException("Cache already started");
        }

        $res = $cache->startDataCache(
            $cacheTime,
            $cacheId,
            static::BASE_CACHE_DIR
        );

        if (!$res) {
            return $cache->getVars();
        }

        $this->cacheStarted = true;

        $iblockId = $this->getId();
        if ($iblockId) {
            //метим наш кеш тегом инфоблока, для автоматической очистки при изменениях
            $this->addCacheTag("iblock_id_{$iblockId}");
        }


        return false;
    }

    /**
     * @return $this
     * @throws RepositoryException
     */
    protected function abortCache()
    {
        if (!$this->isCacheStarted()) {
            throw new RepositoryException("Unable abort cache, because cache not started");
        }

        $this->getCacheInstance()->abortDataCache();
        $this->cacheStarted = false;
        return $this;
    }

    /**
     * @param $vars
     * @return $this
     * @throws RepositoryException
     */
    protected function endCache($vars)
    {
        if (!$this->isCacheStarted()) {
            throw new RepositoryException("Unable finish  cache, because cache aborted or not started");
        }

        $this->getCacheInstance()->endDataCache($vars);
        $this->cacheStarted = false;
        return $this;
    }

    /**
     * @param $tag
     * @return $this
     * @throws RepositoryException
     */
    protected function addCacheTag($tag)
    {
        if (!$this->isCacheStarted()) {
            throw new RepositoryException("Unable add tag to cache, because cache aborted or not started");
        }

        Application::getInstance()->getTaggedCache()->registerTag($tag);
        return $this;
    }

    /**
     * @return Cache
     */
    protected function getCacheInstance()
    {
        if ($this->cacheInstance !== false) {
            return $this->cacheInstance;
        }
        return $this->cacheInstance = Cache::createInstance();
    }

    /**
     * Загружает данные инфоблока
     *
     * @param $iblockCode
     * @param int $cacheTime
     * @return array
     * @throws RepositoryException
     */
    protected function load($iblockCode, $cacheTime = 0)
    {
        $cacheId = "iblock_{$iblockCode}";
        if ($data = $this->getCache($cacheTime, $cacheId)) {
            return $this->iblock = $data['iblock'];
        }

        $row = \CIBlock::GetList([], ['CODE' => $iblockCode])->Fetch();
        if (!$row) {
            $this->abortCache();
            throw new RepositoryException("Iblock with code {$iblockCode} not found");
        }

        $this->endCache(['iblock' => $row]);
        return $this->iblock = $row;
    }

    protected static $repositories = [];

    /**
     * @param $iblockCode
     * @param $cacheTime
     * @return IblockRepositoryInterface
     */
    public static function getInstance($iblockCode, $cacheTime = 0)
    {
        $className = get_called_class();
        $cacheKey = md5($className . '|' . $iblockCode . '|' . $cacheTime);
        if (isset(static::$repositories[$cacheKey])) {
            return static::$repositories[$cacheKey];
        }

        return static::$repositories[$cacheKey] = new static($iblockCode, $cacheTime);
    }

    /**
     * Возвращает ID инфоблока
     *
     * @return integer
     */
    public function getId()
    {
        return $this->iblock['ID'];
    }

    /**
     * Возвращает код инфоблока
     *
     * @return string
     */
    public function getCode()
    {
        return $this->iblock['CODE'];
    }

    /**
     * Возвращает название инфоблока
     *
     * @return string
     */
    public function getName()
    {
        return $this->iblock['NAME'];
    }


    /**
     * Возвращает тип инфоблока
     *
     * @return string
     */
    public function getType()
    {
        return $this->iblock['IBLOCK_TYPE_ID'];
    }

    /**
     * @param $count
     * @return SelectResultInterface
     */
    protected function getResultInstance($count)
    {
        return SelectResult::getInstance($count);
    }
}