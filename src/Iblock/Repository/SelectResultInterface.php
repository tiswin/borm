<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Repository;
use Iswin\Borm\Common\Collection;
use Iswin\Borm\Iblock\IblockEntity;

/**
 * Интерфейс для результатов выборки
 *
 * Interface SelectResultInterface
 * @package Iswin\Borm\Iblock\Repository
 */
interface SelectResultInterface
{
    /**
     * @param $allCount
     * @return SelectResultInterface
     */
    public static function getInstance($allCount);

    /**
     * Добавляет элемент в коллекцию
     *
     * @param array $item
     * @return mixed
     */
    public function addItem($item);

    /**
     * Возвращает выбранные элементы в сыром виде (в виде массива)
     *
     * @return array
     */
    public function getItems();

    /**
     * Возвращает общее кол-во элементов попадающих под фильтр
     *
     * @return mixed
     */
    public function getCount();
}