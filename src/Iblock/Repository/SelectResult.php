<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Repository;

/**
 * Class SelectResult
 * @package Iswin\Borm\Iblock\Repository
 */
class SelectResult implements SelectResultInterface
{

    protected $count = 0;
    protected $items = [];

    protected function __construct ($count)
    {
        $this->count = $count;
    }

    public static function getInstance ($allCount)
    {
        return new static($allCount);
    }

    public function addItem ($item)
    {
        $this->items[] = $item;
        return $this;
    }

    public function getItems ()
    {
        return $this->items;
    }

    public function getCount ()
    {
        return $this->count;
    }
}