<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Repository;

/**
 * Class RepositoryException
 * @package Iswin\Borm\Iblock\Repository
 */
class RepositoryException extends \Exception
{

}