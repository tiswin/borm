<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Repository;

use Iswin\Borm\Iblock\IblockQueryInterface;

/**
 * Интерфейс для репозитория инфоблоков
 *
 * Interface IblockRepositoryInterface
 * @package Iswin\Borm\Iblock
 */
interface IblockRepositoryInterface
{
    /**
     * @param $iblockCode
     * @param $cacheTime
     * @return IblockRepositoryInterface
     */
    public static function getInstance($iblockCode, $cacheTime = 0);

    /**
     * Осуществляет выборку в БД и возвращает сырые результаты в виде массива
     *
     * @param IblockQueryInterface $query
     * @return SelectResultInterface
     */
    public function getList(IblockQueryInterface $query);

    /**
     * Возвращает ID инфоблока
     *
     * @return integer
     */
    public function getId();

    /**
     * Возвращает код инфоблока
     *
     * @return string
     */
    public function getCode();

    /**
     * Возвращает название инфоблока
     *
     * @return string
     */
    public function getName();


    /**
     * Возвращает тип инфоблока
     *
     * @return string
     */
    public function getType();

    /**
     * Возвращает массив свойств элементов или разделов инфоблока
     *
     * @param $cacheTime
     * @return array
     */
    public function getProperties($cacheTime = 0);

    /**
     * Возвращает ассоциативный массив свойств элементов или разделов инфоблока
     *
     * @param $cacheTime
     * @return array
     */
    public function getPropertiesByCodes($cacheTime = 0);
}