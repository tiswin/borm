<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Section;

use Iswin\Borm\Iblock\IblockEntityInterface;

interface SectionEntityInterface extends IblockEntityInterface
{

}