<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Section;


use Iswin\Borm\Exceptions\ExceptionEntity;
use Iswin\Borm\Iblock\IblockEntity;
use Iswin\Borm\Iblock\IblockQueryInterface;

/**
 * Class SectionEntity
 * @package Iswin\Borm\Iblock\Section
 */
abstract class SectionEntity extends IblockEntity implements SectionEntityInterface
{
    const PREG_PROP = "#^UF_.+#Umsi";


    public static function getRepository ()
    {
        return SectionRepository::getInstance(static::getIblockCode());
    }

    /**
     * @see SectionQuery
     *
     * @return SectionQuery
     */
    public static function query ()
    {
        return SectionQuery::getInstance(static::getInstance());
    }

    protected static $saveInstance = false;

    /**
     * @return \CIBlockSection
     */
    protected function getSaveInstance()
    {
        if (self::$saveInstance !== false) {
            return self::$saveInstance;
        }

        return self::$saveInstance = new \CIBlockSection();
    }

    /**
     * Обновляет или добавляет новый раздел
     * Если ID утсановлен - происходит обновление,
     * В противном случае добавление
     *
     * @return $this
     */
    public function save() {
        $instance = $this->getSaveInstance();
        return $this->saveByInstance($instance);
    }

    /**
     * @see IblockEntity::setSectionId()
     *
     * @param $sectionId
     * @return $this
     */
    public function setParentId($sectionId)
    {
        return parent::setSectionId($sectionId);
    }

    /**
     * @see IblockEntityBase::getSectionId()
     *
     * @return int|null
     */
    public function getParentId()
    {
        return parent::getSectionId();
    }

    /**
     * Возвращает ассоциативный массив свойств
     *
     * @return array
     */
    public function getPropsArray()
    {
        $ret = [];
        foreach ($this->data as $key => $val) {
            if (!preg_match(static::PREG_PROP, $key)) {
                continue;
            }

            $ret[$key] = $val;
        }

        return $ret;
    }

    /**
     * Возвращает список родительских разделов
     *
     * @param IblockQueryInterface $query - запрос (нужен для настройки select полей)
     * @return SectionEntity[]
     * @throws ExceptionEntity
     */
    public function getChains(IblockQueryInterface $query)
    {
        $leftMargin = $this->getField('LEFT_MARGIN');
        $rightMargin = $this->getField('RIGHT_MARGIN');
        $depthLevel = $this->getField('DEPTH_LEVEL');
        if (!$leftMargin) {
            throw new ExceptionEntity('Unable get chains for section, because LEFT_MARGIN not select in section');
        }
        if (!$rightMargin) {
            throw new ExceptionEntity('Unable get chains for section, because RIGHT_MARGIN not select in section');
        }
        if (!$depthLevel) {
            throw new ExceptionEntity('Unable get chains for section, because DEPTH_LEVEL not select in section');
        }

        $rows = $query
            ->setOrder(['DEPTH_LEVEL' => 'asc'])
            ->addFilter('<=LEFT_BORDER', $leftMargin)
            ->addFilter('>=RIGHT_BORDER', $rightMargin)
            ->addFilter('<DEPTH_LEVEL', $depthLevel)
            ->fetchAll();

        return $rows;
    }
}