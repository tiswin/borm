<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Section;


use Iswin\Borm\Iblock\IblockQueryInterface;

interface SectionQueryInterface extends IblockQueryInterface
{

    public function getCnt();
}