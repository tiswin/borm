<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Section;


use Iswin\Borm\Iblock\Element\ElementEntity;
use Iswin\Borm\Iblock\IblockEntity;
use Iswin\Borm\Iblock\IblockEntityInterface;
use Iswin\Borm\Iblock\LinkManager;
use Iswin\Borm\Iblock\Query;

/**
 * Class SectionQuery
 * @package Iswin\Borm\Iblock\Section
 */
class SectionQuery extends Query implements SectionQueryInterface
{

    /**
     * Возвращает, нужно ли высчитывать количество элементов в раздела
     * @link https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblocksection/getlist.php
     * bIncCnt
     *
     * @return mixed
     */
    public function getCnt ()
    {
        return $this->cnt;
    }

    /**
     * Устанавливает режим подсчета элементов в разделе
     * @link https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblocksection/getlist.php
     * bIncCnt
     *
     * @param $cnt
     * @return $this
     */
    public function setCnt ($cnt)
    {
        $this->cnt = $cnt;
        return $this;
    }

    /**
     * @param IblockEntity $entity
     * @return SectionQuery
     */
    public static function getInstance (IblockEntityInterface $entity)
    {
        return parent::getInstance($entity);
    }

    /**
     * Добавляет в выборку пользовательское свойство по его коду
     *
     * @param $propCode
     * @return $this
     */
    public function addSelectProp($propCode)
    {
        $this->addSelect($propCode);
        return $this;
    }

    /**
     * Добавляет в выборку url раздела
     *
     * @return $this
     */
    public function addSelectUrl()
    {
        $this->addSelect('SECTION_PAGE_URL');
        return $this;
    }


    /**
     * Добавляет в выборку все поля раздела и все его свойства
     *
     * @return $this
     */
    public function selectAll()
    {
        $this->addSelect('IBLOCK_ID')
            ->addSelect('ID')
            ->addSelect('CODE')
            ->addSelect('NAME')
            ->addSelect('DESCRIPTION')
            ->addSelect('PICTURE')
            ->addSelect('SORT')
            ->addSelect('ACTIVE')
            ->addSelect('IBLOCK_SECTION_ID')
            ->addSelect('DETAIL_PICTURE')
            ->addSelectUrl()
            ->addSelectProp('*');

        return $this;
    }

    /**
     * @param $select
     * @return $this|SectionQuery
     */
    public function setSelect ($select)
    {
        if ($select == ['*']) {
            return $this->selectAll();
        }
        return parent::setSelect($select);
    }

    /**
     * @return SectionEntity[]
     */
    public function fetchAll ()
    {
        $results = parent::fetchAll();
        if ($this->linkedManagers) {
            $this->loadlinkedObjects($results);
        }

        return $results;
    }


    protected function loadlinkedObjects($results)
    {
        $linkedManagers = $this->linkedManagers;

        /** @var SectionEntity $result */
        foreach ($results as $result) {
            /** @var LinkManager $linkedManager */
            foreach ($linkedManagers as $linkedManager) {
                $propCode = $linkedManager->getPropertyCode();
                $linkIds = $result->getField($propCode);
                if (!$linkIds) {
                    continue;
                }
                if (!is_array($linkIds)) {
                    $linkIds = [$linkIds];
                }
                foreach ($linkIds as $linkId) {
                    $linkedManager->addLink($linkId, $result);
                }
            }
        }


        foreach ($linkedManagers as $linkedManager) {
            $linkedManager->exec('ID');
        }
    }


    /**
     * @return SectionEntity
     */
    public function getOneResult ()
    {
        return parent::getOneResult();
    }
}