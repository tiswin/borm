<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Section;


use Iswin\Borm\Common\Collection;
use Iswin\Borm\Iblock\IblockQueryInterface;
use Iswin\Borm\Iblock\PageNavInterface;
use Iswin\Borm\Iblock\Query;
use Iswin\Borm\Iblock\Repository\IblockRepository;
use Iswin\Borm\Iblock\Repository\SelectResultInterface;

/**
 * Class SectionRepository
 * @package Iswin\Borm\Iblock\Section
 */
class SectionRepository extends IblockRepository
{

    /**
     * @param int $cacheTime
     * @return bool|Collection
     */
    public function getProperties ($cacheTime = 0)
    {
        /** @todo надо дописать получение свойств раздела */
        return Collection::getInstance([]);
    }

    /**
     * Возвращает свойства ассоцированные по их кодам
     * @param $cacheTime
     * @return array|bool
     */
    public function getPropertiesByCodes($cacheTime = 0)
    {

        $ret = [];

        $properties = $this->getProperties($cacheTime);
        foreach ($properties as $property) {
            $ret[$property['CODE']] = $property;
        }

        return Collection::getInstance($ret);
    }

    /**
     * @param SectionQueryInterface $query
     * @return SelectResultInterface
     */
    public function getList (IblockQueryInterface $query)
    {
        $order = $query->getOrder() ? : [];
        $select = $query->getSelect() ? : ['ID', 'IBLOCK_ID'];
        $filter = $query->getFilter() ? : [];

        if (!isset($filter['IBLOCK_ID'])) {
            $filter['IBLOCK_ID'] = $this->getId();
        }

        ksort($select);
        ksort($filter);

        $cnt = $query->getCnt() ? : false;

        /** @var PageNavInterface|bool $nav */
        $nav = $query->getNav() ? : false;
        $navParams = false;
        if ($nav) {
            $navParams = $nav->toArray();
            ksort($navParams);
        }

        $cacheId = md5(
            serialize($order) .
            serialize($select) .
            serialize($filter) .
            serialize($cnt) .
            serialize($navParams)
        );

        if ($data = $this->getCache($query->getCacheTime(), $cacheId)) {
            return $data['result'];
        }

        if ($tags = $query->getCacheTag()) {
            if (!is_array($tags)) {
                $tags = [$tags];
            }
            foreach ($tags as $tag) {
                $tag = trim($tag);
                if (!$tag) {
                    continue;
                }
                $this->addCacheTag($tag);
            }
        }

        $rows = \CIBlockSection::GetList(
            $order,
            $filter,
            $cnt,
            $select,
            $navParams
        );

        $count = $rows->SelectedRowsCount();
        $result = $this->getResultInstance($count);
        while ($row = $rows->GetNext()) {
            $result->addItem($row);
        }
        //в кеш пишем массив, т.к. если писать сразу результат парсинга - можно забить плотно кеш
        /** @todo но подумать, протестировать может быть все таки лучше писать в кеш result */
        $this->endCache(['result' => $result]);
        return $result;
    }

}