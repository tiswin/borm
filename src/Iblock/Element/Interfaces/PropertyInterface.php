<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element\Interfaces;


interface PropertyInterface
{

    public function setValue($value);

    public function isList();

    public function setEnumId($enumId);

    public function getValue();

    public function getEnumId();

    public function isNeedSave();

    public function getPropertyId();

    public function isFile();

    public function isMulti();

    public function getCode();
}