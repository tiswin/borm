<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element\Interfaces;


use Iswin\Borm\Exceptions\ExceptionEntity;

interface CatalogPriceInterface
{

    public function getPrice();
    public function getCurrency();
    public function save();

    /**
     * Устанавливает ID продукта
     *
     * @param $productId
     * @return $this
     */
    public function setProductId($productId);

    /**
     * Возвращает ID продукты
     *
     * @return mixed
     */
    public function getProductId();

    /**
     * Устанавливает значение поля цены по его коду
     *
     * @param $key
     * @param $val
     * @return $this
     */
    public function setField($key, $val);


    /**
     * Это базовая цена?
     *
     * @return bool
     */
    public function isBase();

    /**
     * Вовзращает название типа цены
     *
     * @return mixed
     */
    public function getName();

    /**
     * Возвращает ID типа цены
     *
     * @return mixed
     */
    public function getPriceGroupId();

    /**
     * Устанавливает тип цены
     *
     * @param $groupId
     * @return $this
     */
    public function setPriceGroupId($groupId);

    /**
     * Возвращает ID цены
     *
     * @return mixed
     */
    public function getPriceId();

    /**
     * Устанаваливает ID цены
     *
     * @param $id
     * @return $this
     */
    public function setPriceId($id);

    /**
     * Устанавливает тип цены
     *
     * @param $type - ассцоиативный массив с полями типа цены
     * @return $this
     */
    public function setType($type);

    /**
     * Устанаваливает значение цены
     *
     * @param $price
     * @return $this
     */
    public function setPrice($price);

    /**
     * Устанавливает валюту
     *
     * @param $currency
     * @return $this
     */
    public function setCurrency($currency);

    /**
     * @return mixed
     */
    public function getExtraId();

    /**
     * @return mixed
     */
    public function getQuantityFrom();

    /**
     * @return mixed
     */
    public function getQuantityTo();
}