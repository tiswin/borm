<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element\Interfaces;


use Iswin\Borm\Exceptions\ExceptionEntity;

interface CatalogDataInterface
{
    /**
     * Возвращает ID продукта
     *
     * @return mixed
     */
    public function getProductId();

    /**
     * Устанавлвиает ID продукта
     *
     * @param $productId
     * @return $this
     */
    public function setProductId($productId);

    /**
     * @return CatalogPriceInterface[]|bool
     */
    public function getPrices();

    /**
     * Возвращает тип продукта
     *
     * @see ProductTable::TYPE_PRODUCT
     * @see ProductTable::TYPE_SET
     * @see ProductTable::TYPE_SKU
     * @see ProductTable::TYPE_FREE_OFFER
     * @see ProductTable::TYPE_EMPTY_SKU
     *
     * @return int
     */
    public function getProductType();

    /**
     * Устанавливает тип продукта
     *
     * @see ProductTable::TYPE_PRODUCT
     * @see ProductTable::TYPE_SET
     * @see ProductTable::TYPE_SKU
     * @see ProductTable::TYPE_FREE_OFFER
     * @see ProductTable::TYPE_EMPTY_SKU
     *
     * @param $type
     * @return $this
     */
    public function setProductType($type);

    /**
     * Устанавливает тип продукта - как продукт без торговых предложений
     *
     * @return $this
     */
    public function setProductTypeWithoutOffers();

    /**
     * Устанавливает тип продукта - как продукт c торговыми предложениями
     *
     * @return $this
     */
    public function setProductTypeWithOffers();

    public function getPriceType();

    /**
     * Устснаваливает тип цены продукты
     *
     * @see ProductTable::PAYMENT_TYPE_SINGLE
     * @see ProductTable::PAYMENT_TYPE_REGULAR
     * @see ProductTable::PAYMENT_TYPE_TRIAL
     *
     * @param $priceType
     * @return $this
     */
    public function setPriceType($priceType);

    /**
     * Устанавливает обычный тип цен продукта
     *
     * @return $this
     */
    public function setPriceTypeSingle();

    /**
     * Устанаваливает доступное кол-во товара
     *
     * @param $count
     * @return $this
     */
    public function setQuantity($count);

    /**
     * Возвращает доступное количество товара
     *
     * @return mixed
     */
    public function getQuantity();


    /**
     * Добавляет новую цену для продукта
     * Внимание! Этим методом добавляются только новые цены, если вы хотите изменить существующую цену,
     * используйте:
     * <code>
     *   $prices = $catalogData->getPrices();
     *   //..... Выбираем из массива нужную нам цену, если нужна базовая цена то можно получить ее
     *   // сразу: $price = $catalogData->getBasePrice();
     *   $price->setPrice('124321')
     *      ->setCurrency('USD')
     *      ->save();
     * </code>
     *
     *
     * @param CatalogPriceInterface $price
     * @return $this
     */
    public function addPrice(CatalogPriceInterface $price);


    /**
     * Возвращает объект базовой цены продукта
     *
     * @return CatalogPriceInterface|null
     */
    public function getBasePrice();

    /**
     * Создает продукт, если он еще не создан, а также сохраняет все значения цен
     *
     * @throws ExceptionEntity
     * @return $this
     */
    public function save();
}