<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;

use Iswin\Borm\Iblock\Element\Interfaces\PropertyInterface;


/**
 * Объект значение свойства элемента инфоблока
 *
 * Class Property
 * @package Iswin\Borm\Iblock\Element
 */
class Property implements PropertyInterface
{
    protected $code;
    protected $name;
    protected $data;
    protected $type;
    protected $propertyId;
    protected $needSave = false;

    protected function __construct ($code)
    {
        $this->code = $code;
    }

    /**
     * @param $code
     * @return Property
     */
    public static function getInstance($code)
    {
        return new static($code);
    }

    public function setField($field, $value)
    {
        $this->data[$field] = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPropertyId ()
    {
        return $this->propertyId;
    }

    /**
     * @param $propertyId
     * @return $this
     */
    public function setPropertyId ($propertyId)
    {
        $this->propertyId = $propertyId;
        return $this;
    }

    /**
     * Возвращает тип свойства
     *
     * @return mixed
     */
    public function getType ()
    {
        return $this->data['PROPERTY_TYPE'];
    }

    /**
     * Устанавливает тип свойства
     *
     * @param $type
     * @return $this
     */
    public function setType ($type)
    {
        $this->data['PROPERTY_TYPE'] = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCode ()
    {
        return $this->code;
    }

    /**
     * @param $code
     * @return $this
     */
    public function setCode ($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName ($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue ()
    {
        return $this->data['VALUE'];
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue ($value)
    {
        $this->needSave = true;
        $this->data['VALUE'] = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValueId ()
    {
        return $this->data['VALUE_ID'];
    }

    /**
     * @param $valueId
     * @return $this
     */
    public function setValueId ($valueId)
    {
        $this->data['VALUE_ID'] = $valueId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnumId ()
    {
        return $this->data['ENUM_ID'];
    }

    /**
     * @param $enumId
     * @return $this
     */
    public function setEnumId ($enumId)
    {
        $this->needSave = true;
        $this->data['ENUM_ID'] = $enumId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription ()
    {
        return $this->data['DESCRIPTION'];
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription ($description)
    {
        $this->needSave = true;
        $this->data['DESCRIPTION'] = $description;
        return $this;
    }

    /**
     * Свойство типа файл?
     *
     * @return bool
     */
    public function isFile()
    {
        return $this->getType() == 'F';
    }

    /**
     * Множественное свойство?
     */
    public function isMulti()
    {
        return $this->data['MULTIPLE'] = 'Y';
    }

    /**
     * Свойство типа список?
     *
     * @return bool
     */
    public function isList()
    {
        return $this->getType() == 'L';
    }

    public function isNeedSave()
    {
        return $this->needSave;
    }
}