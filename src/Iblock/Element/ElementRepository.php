<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;


use Bitrix\Iblock\PropertyTable;
use Iswin\Borm\Common\Collection;
use Iswin\Borm\Common\QueryInterface;
use Iswin\Borm\Iblock\IblockQueryInterface;
use Iswin\Borm\Iblock\PageNavInterface;
use Iswin\Borm\Iblock\Query;
use Iswin\Borm\Iblock\Repository\IblockRepository;
use Iswin\Borm\Iblock\Repository\SelectResultInterface;

/**
 * Class ElementRepository
 * @package Iswin\Borm\Iblock\Element
 */
class ElementRepository extends IblockRepository
{

    /**
     * Регулярное выражение для получение полей каталога
     */
    const PREG_CATALOG_FIELD = "#^CATALOG_(.+)$#Umsi";
    /**
     * Регулярное выражение для получение полей свойств
     */
    const PREG_PROPERTY_FIELD = "#^PROPERTY_([0-9]+)_(.+)?$#Umsi";

    protected $properties = false;

    /**
     * @param int $cacheTime
     * @return bool|Collection
     */
    public function getProperties ($cacheTime = 0)
    {
        if ($this->properties !== false) {
            return $this->properties;
        }

        $cacheId = "properties_element_{$this->getId()}";

        if ($data = $this->getCache($cacheTime, $cacheId)) {
            return $this->properties = Collection::getInstance($data['props']);
        }

        $rows = PropertyTable::query()
            ->addSelect('*')
            ->addFilter('=IBLOCK_ID', $this->getId())
            ->exec();

        $ret = [];

        while ($row = $rows->fetch()) {
            $ret[$row['ID']] = $row;
        }

        $this->endCache(['props' => $ret]);
        return $this->properties = Collection::getInstance($ret);
    }

    /**
     * Возвращает свойства ассоцированные по их кодам
     * @param $cacheTime
     * @return array|bool
     */
    public function getPropertiesByCodes($cacheTime = 0)
    {

        $ret = [];

        $properties = $this->getProperties($cacheTime);
        foreach ($properties as $property) {
            $ret[$property['CODE']] = $property;
        }

        return Collection::getInstance($ret);
    }


    protected function parseRow($row)
    {
        $ret = [];
        $catalogFields = [];
        $iblockProps = $this->getProperties();

        $propertyCollection = PropsCollection::getInstance(
            $row['ID'],
            $row['IBLOCK_ID'],
            $this->getPropertiesByCodes()
        );

        foreach ($row as $key => $val) {
            if ($key[0] == '~') {
                continue;
            }

            if (preg_match(static::PREG_CATALOG_FIELD, $key, $matches)) {
                $catalogFields[$matches[1]] = $val;
                continue;
            }

            if (preg_match(static::PREG_PROPERTY_FIELD, $key, $matches)) {

                $propId = $matches[1];

                $property = $iblockProps[$propId];
                if (!$property) {
                    continue;
                }

                $propertyCode = $property['CODE'];

                $propInstance = $propertyCollection->getProperty($propertyCode);
                if (!$propInstance) {
                    continue;
                }

                $fieldCode = $matches[2];

                $propInstance->setField($fieldCode, $val);
                continue;
            }

            $ret[$key] = $val;
        }

        if ($catalogFields) {
            $ret['CATALOG_DATA'] = CatalogData::getInstance($catalogFields, $ret['ID']);
        }

        $ret['PROP_COLLECTION'] = $propertyCollection;

        return $ret;
    }



    /**
     * @param ElementQueryInterface $query
     * @return SelectResultInterface
     */
    public function getList (IblockQueryInterface $query)
    {
        $order = $query->getOrder() ? : [];
        $select = $query->getSelect() ? : ['ID', 'IBLOCK_ID'];
        $filter = $query->getFilter() ? : [];
        if (!isset($filter['IBLOCK_ID'])) {
            $filter['IBLOCK_ID'] = $this->getId();
        }

        ksort($select);
        ksort($filter);

        $groupBy = $query->getGroup() ? : false;
        if ($groupBy) {
            ksort($groupBy);
        }


        /** @var PageNavInterface|bool $nav */
        $nav = $query->getNav() ? : false;
        $navParams = false;
        if ($nav) {
            $navParams = $nav->toArray();
            ksort($navParams);
        }

        $cacheId = md5(
            serialize($order) .
            serialize($select) .
            serialize($filter) .
            serialize($groupBy) .
            serialize($navParams)
        );

        if ($data = $this->getCache($query->getCacheTime(), $cacheId)) {
            return $this->makeSelectedResult($data['rows'], $data['count']);
        }

        if ($tags = $query->getCacheTag()) {
            if (!is_array($tags)) {
                $tags = [$tags];
            }
            foreach ($tags as $tag) {
                $tag = trim($tag);
                if (!$tag) {
                    continue;
                }
                $this->addCacheTag($tag);
            }
        }

        $rows = \CIBlockElement::GetList(
            $order,
            $filter,
            $groupBy,
            $navParams,
            $select
        );

        $count = $rows->SelectedRowsCount();
        $ret = [];
        while ($row = $rows->GetNext()) {
            $ret[] = $row;
        }
        //в кеш пишем массив, т.к. если писать сразу результат парсинга - можно забить плотно кеш
        /** @todo протестировать может быть все таки лучше писать в кеш result */
        $this->endCache(['rows' => $ret, 'count' => $count]);
        return $this->makeSelectedResult($ret, $count);
    }

    protected function makeSelectedResult($rows, $count)
    {
        $result = $this->getResultInstance($count);
        foreach ($rows as $row) {
            $result->addItem($this->parseRow($row));
        }

        return $result;
    }
}