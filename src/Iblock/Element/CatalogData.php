<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;
use Bitrix\Catalog\ProductTable;
use Bitrix\Main\Loader;
use Iswin\Borm\Exceptions\ExceptionEntity;
use Iswin\Borm\Iblock\Element\Interfaces\CatalogDataInterface;
use Iswin\Borm\Iblock\Element\Interfaces\CatalogPriceInterface;

Loader::includeModule('catalog');

/**
 * Класс для работы с данными каталога элемента инфоблока
 *
 * Class CatalogData
 * @package Iswin\Borm\Iblock\Element
 */
class CatalogData implements CatalogDataInterface
{
    /**
     * регулярное выражение для выборки полей цен
     */
    const PREG_PRICE_DATA = "#^(.+)_([0-9]+)$#";

    protected $data;
    protected $productId;

    protected $prices = false;

    protected function __construct ($data, $productId)
    {
        $this->data = $data;
        $this->productId = $productId;
    }

    public static function getInstance($data, $productId)
    {
        return new static($data, $productId);
    }

    /**
     * Возвращает ID продукта
     *
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Устанавлвиает ID продукта
     *
     * @param $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        foreach ($this->getPrices() as $price) {
            $price->setProductId($productId);
        }
        return $this;
    }

    /**
     * @return CatalogPrice[]|bool
     */
    public function getPrices()
    {
        if ($this->prices !== false) {
            return $this->prices;
        }

        $priceTypes = ElementEntity::getPriceTypes();
        $prices = [];

        foreach ($this->data as $key => $val) {
            if (!preg_match(static::PREG_PRICE_DATA, $key, $matches)) {
                continue;
            }

            $priceGroupId = $matches[2];
            if (!isset($prices[$priceGroupId])) {
                $priceType = $priceTypes[$priceGroupId];
                if (!$priceType) {
                    continue;
                }
                $prices[$priceGroupId] = CatalogPrice::getInstance($this->getProductId(), ['TYPE' => $priceType]);
            }
            /** @var CatalogPrice $priceInstance */
            $priceInstance = $prices[$priceGroupId];
            $priceInstance->setField($matches[1], $val);
        }

        return $this->prices = $prices;
    }

    /**
     * Возвращает тип продукта
     *
     * @see ProductTable::TYPE_PRODUCT
     * @see ProductTable::TYPE_SET
     * @see ProductTable::TYPE_SKU
     * @see ProductTable::TYPE_FREE_OFFER
     * @see ProductTable::TYPE_EMPTY_SKU
     *
     * @return int
     */
    public function getProductType()
    {
        return $this->data['TYPE'];
    }

    /**
     * Устанавливает тип продукта
     *
     * @see ProductTable::TYPE_PRODUCT
     * @see ProductTable::TYPE_SET
     * @see ProductTable::TYPE_SKU
     * @see ProductTable::TYPE_FREE_OFFER
     * @see ProductTable::TYPE_EMPTY_SKU
     *
     * @param $type
     * @return $this
     */
    public function setProductType($type)
    {
        $this->data['TYPE'] = $type;
        return $this;
    }

    /**
     * Устанавливает тип продукта - как продукт без торговых предложений
     *
     * @return $this
     */
    public function setProductTypeWithoutOffers()
    {
        return $this->setProductType(ProductTable::TYPE_PRODUCT);
    }

    /**
     * Устанавливает тип продукта - как продукт c торговыми предложениями
     *
     * @return $this
     */
    public function setProductTypeWithOffers()
    {
        return $this->setProductType(ProductTable::TYPE_SKU);
    }

    public function getPriceType()
    {
        return $this->data['PRICE_TYPE'];
    }

    /**
     * Устснаваливает тип цены продукты
     *
     * @see ProductTable::PAYMENT_TYPE_SINGLE
     * @see ProductTable::PAYMENT_TYPE_REGULAR
     * @see ProductTable::PAYMENT_TYPE_TRIAL
     *
     * @param $priceType
     * @return $this
     */
    public function setPriceType($priceType)
    {
        $this->data['PRICE_TYPE'] = $priceType;
        return $this;
    }

    /**
     * Устанавливает обычный тип цен продукта
     *
     * @return $this
     */
    public function setPriceTypeSingle()
    {
        $this->setPriceType(ProductTable::PAYMENT_TYPE_SINGLE);
        return $this;
    }

    /**
     * Устанаваливает доступное кол-во товара
     *
     * @param $count
     * @return $this
     */
    public function setQuantity($count)
    {
        $this->data['QUANTITY'] = $count;
        return $this;
    }

    /**
     * Возвращает доступное количество товара
     *
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->data['QUANTITY'];
    }


    /**
     * Добавляет новую цену для продукта
     * Внимание! Этим методом добавляются только новые цены, если вы хотите изменить существующую цену,
     * используйте:
     * <code>
     *   $prices = $catalogData->getPrices();
     *   //..... Выбираем из массива нужную нам цену, если нужна базовая цена то можно получить ее
     *   // сразу: $price = $catalogData->getBasePrice();
     *   $price->setPrice('124321')
     *      ->setCurrency('USD')
     *      ->save();
     * </code>
     *
     *
     * @param CatalogPriceInterface $price
     * @return $this
     */
    public function addPrice(CatalogPriceInterface $price)
    {
        $this->getPrices();
        if (!is_array($this->prices)) {
            $this->prices = [];
        }

        $this->prices[] = $price;
        return $this;
    }


    /**
     * Возвращает объект базовой цены продукта
     *
     * @return CatalogPriceInterface|null
     */
    public function getBasePrice()
    {
        $prices = $this->getPrices();
        foreach ($prices as $price) {
            if ($price->isBase()) {
                return $price;
            }
        }

        return null;
    }

    /**
     * Создает продукт, если он еще не создан, а также сохраняет все значения цен
     *
     * @throws ExceptionEntity
     * @return $this
     */
    public function save()
    {
        $productId = $this->getProductId();
        $productType = $this->getProductType();

        if (!$productId) {
            $e = new ExceptionEntity("Попытка сохранить продукт для не существующего элемента инфоблока");
            throw $e->setData($this->data);
        }

        if (!$productType) {
            $e = new ExceptionEntity("Попытка сохранить продукт без типа");
            throw $e->setData($this->data);
        }


        $data = [
            'ID' => $productId,
            'TYPE' => $productType
        ];

        $count = $this->getQuantity();
        if (!is_null($count)) {
            $data['QUANTITY'] = $count;
        }

        if ($priceType = $this->getPriceType()) {
            $data['PRICE_TYPE'] = $priceType;
        }

        if (ProductTable::isExistProduct($productId)) {
            $res = ProductTable::update($productId, $data);
        } else {
            $res = ProductTable::add($data);
        }

        if (!$res->isSuccess()) {
            $message = implode("; ", $res->getErrorMessages());
            $e = new ExceptionEntity($message);
            throw $e->setData($data);
        }
        /** @var CatalogPriceInterface $price */
        foreach ($this->prices as $price) {
            if ($price->getPrice() > 0 && $price->getCurrency()) {
                $price->save();
            }
        }

        return $this;
    }
}