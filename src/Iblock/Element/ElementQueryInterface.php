<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;


use Iswin\Borm\Iblock\IblockQueryInterface;

interface ElementQueryInterface extends IblockQueryInterface
{
    public function getGroup();
}