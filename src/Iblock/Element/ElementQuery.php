<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;


use Iswin\Borm\Common\QueryInterface;
use Iswin\Borm\Iblock\IblockEntityInterface;
use Iswin\Borm\Iblock\LinkManager;
use Iswin\Borm\Iblock\Query;

/**
 * Class ElementQuery
 * @package Iswin\Borm\Iblock\Element
 */
class ElementQuery extends  Query implements ElementQueryInterface
{


    /**
     * Регулярное выражение для получения кода свойства из массива select
     */
    const PREG_PROPERTY_CODE = '#^PROPERTY_([^\*]+)(_VALUE|_XML_ID)?$#';
    /**
     * код для выбора раздела элемента, он должен совпадать с кодом какого-либо свойства инфоблока
     */
    const SECTION_LINK_CODE = '__SECTION__';

    protected $properties;

    /**
     * Возвращает параметры группировки
     * @link https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockelement/getlist.php
     * arGroupBy
     *
     * @return mixed
     */
    public function getGroup ()
    {
        return $this->group;
    }

    /**
     * Устанавливае параметры группировки
     * @link https://dev.1c-bitrix.ru/api_help/iblock/classes/ciblockelement/getlist.php
     * arGroupBy
     *
     * @param $group
     * @return $this
     */
    public function setGroup ($group)
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @param ElementEntity $entity
     * @return ElementQuery
     */
    public static function getInstance (IblockEntityInterface $entity)
    {
        return parent::getInstance($entity);
    }

    /**
     * Устанавлвиает список свойств инфоблока
     *
     * @param $properties
     * @return $this
     */
    public function setProperties($properties)
    {
        $this->properties = $properties;
        return $this;
    }

    protected function getProperties()
    {
        return $this->properties;
    }

    /**
     * Добавляет в выборку свойство по его коду
     *
     * @param $propCode
     * @return $this
     */
    public function addSelectProp($propCode)
    {
        if ($propCode == static::SECTION_LINK_CODE) {
            $this->addSelect('IBLOCK_SECTION_ID');
        } else {
            $this->addSelect("PROPERTY_" . $propCode);
        }

        return $this;
    }

    /**
     * @return ElementEntity
     */
    protected function getEntity ()
    {
        return parent::getEntity();
    }

    /**
     * @return $this
     */
    public function addSelectUrl()
    {
        $this->addSelect('DETAIL_PAGE_URL');
        return $this;
    }

    /**
     * Добавляет в выборку цены и данные каталога
     *
     * @return $this
     */
    public function selectCatalog()
    {
        $priceTypes = $this->getEntity()::getPriceTypes();
        if (!$priceTypes) {
            return $this;
        }

        foreach ($priceTypes as $priceType) {
            $this->addSelect('CATALOG_GROUP_' . $priceType['ID']);
        }

        return $this;
    }


    /**
     * Добавляет в выборку все поля элемента, все его свойства и все его цены
     * ВНИМАНИЕ! В выборку не попадут связанные объекты, для их добавления нужно
     * выполнить:
     * <code>
     * $row = Product::query()
     *   ->addSelect('ID')
     *   ->addSelectLink(
     *       'BRAND',
     *       Brand::query()
     *           ->addSelect('ID')
     *           ->addSelect('NAME')
     *           ->addSelect('CODE')
     *   )
     *   ->addFilter('=ID', $productId)
     *   ->getOneResult();
     * </code>
     *
     * @return $this
     */
    public function selectAll()
    {
        $this->addSelect('IBLOCK_ID')
            ->addSelect('ID')
            ->addSelect('CODE')
            ->addSelect('NAME')
            ->addSelect('PREVIEW_TEXT')
            ->addSelect('DETAIL_TEXT')
            ->addSelect('PREVIEW_PICTURE')
            ->addSelect('DETAIL_PICTURE')
            ->addSelect('SORT')
            ->addSelect('ACTIVE')
            ->addSelect('IBLOCK_SECTION_ID')
            ->addSelectUrl()
            ->selectCatalog();

        $props = $this->getEntity()::getProperties();
        foreach ($props as $prop) {
            $this->addSelectProp($prop['CODE']);
        }

        return $this;
    }

    /**
     * Добавляет в выборку раздел элемента инфоблока
     *
     * @param QueryInterface $query
     *
     * @return $this
     */
    public function selectSection(QueryInterface $query)
    {
        $this->addSelectLink(
            static::SECTION_LINK_CODE,
            $query
        );

        return $this;
    }

    /**
     * @return ElementEntity[]
     */
    public function fetchAll ()
    {
        $selects = $this->getSelect();

        foreach ($selects as $index => $select) {
            if (!preg_match(static::PREG_PROPERTY_CODE, $select, $matches)) {
                continue;
            }

            $code = $matches[1];
            $property = $this->getEntity()::getPropertyByCode($code);

            if (!$property) {
                continue;
            }

            $selects[$index] = "PROPERTY_{$property['ID']}";
        }

        $this->setSelect($selects);

        $results = parent::fetchAll();

        if ($this->linkedManagers) {
            $this->loadlinkedObjects($results);
        }

        return $results;
    }

    /**
     * @param $select
     * @return $this|ElementQuery
     */
    public function setSelect ($select)
    {
        if ($select == ['*']) {
            return $this->selectAll();
        }
        return parent::setSelect($select);
    }

    protected function loadlinkedObjects($results)
    {
        $linkedManagers = $this->linkedManagers;


        /** @var ElementEntity $result */
        foreach ($results as $result) {
            /** @var LinkManager $linkedManager */
            foreach ($linkedManagers as $linkedManager) {
                $propCode = $linkedManager->getPropertyCode();

                if ($propCode != static::SECTION_LINK_CODE) {
                    $linkIds = $result->getPropsCollection()->getValue($propCode);
                } else {
                    $linkIds = $result->getSectionIds();
                }

                if (!$linkIds) {
                    continue;
                }
                if (!is_array($linkIds)) {
                    $linkIds = [$linkIds];
                }
                foreach ($linkIds as $linkId) {
                    $linkedManager->addLink($linkId, $result);
                }
            }
        }

        foreach ($linkedManagers as $linkedManager) {
            $linkedManager->exec();
        }
    }

    /**
     * @return ElementEntity
     */
    public function getOneResult ()
    {
        return parent::getOneResult();
    }

}