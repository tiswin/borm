<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;
use Bitrix\Main\Application;
use Iswin\Borm\Exceptions\ExceptionEntity;
use Iswin\Borm\Iblock\Element\Interfaces\PropertyInterface;


/**
 * Объект коллекция свойств
 *
 * Class PropsCollection
 * @package Iswin\Borm\Iblock\Element
 */
class PropsCollection
{
    /**
     * @var Property[]
     */
    protected $props = [];

    protected $iblockProps = [];
    protected $elementId;
    protected $iblockId;

    protected function __construct ($elementId, $iblockId, $iblockProps = [])
    {
        $this->elementId = $elementId;
        $this->iblockId = $iblockId;
        $this->iblockProps = $iblockProps;
    }

    /**
     * Устанаваливает ID элемента
     *
     * @param $id
     * @return $this
     */
    public function setElementId($id)
    {
        $this->elementId = $id;
        return $this;
    }

    /**
     * Возвращает инстанс коллекции свойств
     *
     * @param int|bool  $elementId - ID элемента инфоблока, если элемент новый false
     * @param int $iblockId - ID иинфоблока
     * @param array $iblockProps - свойства инфоблока
     *
     * @see IblockEntityBase::getPropertiesByCodes()
     *
     * @return PropsCollection
     */
    public static function getInstance($elementId, $iblockId, $iblockProps)
    {
        return new PropsCollection($elementId, $iblockId, $iblockProps);
    }

    /**
     * Добавляет свойство в коллекцию
     *
     * @param PropertyInterface $property
     * @return $this
     */
    public function addProp(PropertyInterface $property)
    {
        $this->props[$property->getCode()] = $property;
        return $this;
    }

    /**
     * @param $code
     * @return PropertyInterface|null
     */
    public function getProperty($code)
    {
        if (isset($this->props[$code])) {
            return $this->props[$code];
        }

        $property = $this->iblockProps[$code];
        if (!$property) {
            return null;
        }


        $property = Property::getInstance($code)
            ->setName($property['NAME'])
            ->setPropertyId($property['ID'])
            ->setType($property['PROPERTY_TYPE']);

        return $this->props[$code] = $property;
    }

    /**
     * @param $code
     * @param $value
     * @return $this
     */
    public function setValue($code, $value)
    {

        $property = $this->getProperty($code);
        $property->setValue($value);
        if ($property->isList()) {

            $enums = $this->getEnumsByCode($code);
            if ($enumId = $enums[$value]) {
                return $this->setEnumId($code, $enumId);
            }

            if (!$value) {
                return $this->setEnumId($code, null);
            }
        }
        return $this;
    }

    /**
     * @param $code
     * @param $enumId
     * @return $this
     */
    public function setEnumId($code, $enumId)
    {
        $this->getProperty($code)->setEnumId($enumId);
        return $this;
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getValue($code)
    {
        $property = $this->getProperty($code);
        if (!$property) {
            return null;
        }
        $value = $property->getValue();
        if ($property->isList()) {
            $value = $this->getEnumId($code);
            $enums = $this->getEnumsByCode($code);
            foreach ($enums as $xmlId => $enumId) {
                if ($enumId == $value) {
                    return $xmlId;
                }
            }
        }
        return $value;
    }

    /**
     * Возвращает ассоциативный всех свойств
     *
     * @return array
     */
    public function getAllProps()
    {
        $ret = [];
        foreach ($this->props as $prop) {
            $ret[$prop->getCode()] = $prop->getValue();
        }

        return $ret;
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getEnumId($code)
    {
        return $this->getProperty($code)->getEnumId();
    }

    /**
     * @todo метод не совершенен надо продумать все возможные варианты значения свойств
     * добавить поддержку описания значений и тд.
     *
     * @throws ExceptionEntity
     * @return $this
     */
    public function save()
    {

        $ret = [];

        $fileProps = [];

        /** @var PropertyInterface $prop */
        foreach ($this->props as $prop) {

            if (!$prop->isNeedSave()) {
                continue;
            }

            $value = $prop->isList() ? $prop->getEnumId() : $prop->getValue();

            if ($prop->isFile()) {
                $propertyId = $prop->getPropertyId();
                $fileProps[$propertyId] = $value;
                continue;
            }

            if ($prop->isMulti() && !$value) {
                $value = false;
            }

            $ret[$prop->getCode()] = $value;
        }

        if (!$this->elementId) {
            $message = "Попытка сохранить значения свойств без ID элемента инфоблока";
            $data = $ret;
            $data['ID'] = $this->elementId;
            $data['IBLOCK_ID'] = $this->iblockId;
            $e = new ExceptionEntity($message);
            throw $e->setData($data);
        }

        if ($ret) {
            \CIBlockElement::SetPropertyValuesEx($this->elementId, $this->iblockId, $ret);
        }

        /**
         * @todo но это лютый треш, битрикс не хочет принимать ID старого файла, а при каждом апдейте создает новый
         * поэтому собираем ID файлов в отдельную кучу и делаем прямой запрос
         */
        if ($fileProps) {
            $connection = Application::getConnection();

            foreach ($fileProps as $propId => $fileIds) {
                $propId = (int)$propId;
                if (!$propId) {
                    continue;
                }
                if (is_array($fileIds)) {
                    $tableName = "b_iblock_element_prop_m{$this->iblockId}";
                    $clearSql = "DELETE FROM {$tableName} WHERE IBLOCK_ELEMENT_ID={$this->elementId} AND IBLOCK_PROPERTY_ID={$propId};";
                    $connection->query($clearSql);


                    $fields = "IBLOCK_ELEMENT_ID, IBLOCK_PROPERTY_ID, VALUE, VALUE_NUM";
                    $values = [];

                    $valuesForSave = [
                        'VALUE' => [],
                        'DESCRIPTION' => [],
                        'ID' => []
                    ];

                    foreach ($fileIds as $fileId) {
                        $fileId = (int)$fileId;
                        if (!$fileId) {
                            continue;
                        }
                        $values[] = "({$this->elementId}, {$propId}, {$fileId}, {$fileId})";
                        $valuesForSave['VALUE'][] = $fileId;
                        $valuesForSave['DESCRIPTION'][] = null;
                        $valuesForSave['ID'][] = null;
                    }

                    if (!$values) {
                        continue;
                    }

                    $insertSql = "INSERT INTO {$tableName} ({$fields}) VALUES " . implode(", ", $values) . ";";
                    $connection->query($insertSql);


                    $tableName = "b_iblock_element_prop_s{$this->iblockId}";
                    $valuesForSave = serialize($valuesForSave);
                    $sql = "UPDATE {$tableName} SET PROPERTY_{$propId}='{$valuesForSave}' WHERE IBLOCK_ELEMENT_ID={$this->elementId};";
                    $connection->query($sql);

                } else {
                    $fileIds = (int)$fileIds;
                    if (!$fileIds) {
                        continue;
                    }
                    $tableName = "b_iblock_element_prop_s{$this->iblockId}";
                    $sql = "UPDATE {$tableName} SET PROPERTY_{$propId}={$fileIds} WHERE IBLOCK_ELEMENT_ID={$this->elementId};";
                    $connection->query($sql);
                }
            }

        }

        return $this;
    }


    protected static $enums = [];

    protected function getEnumsByCode($code)
    {
        if (isset(self::$enums[$code])) {
            return self::$enums[$code];
        }

        $property = $this->iblockProps[$code];
        if ($property['PROPERTY_TYPE'] != 'L') {
            return null;
        }

        $ret = [];

        $rows = \CIBlockPropertyEnum::GetList(['SORT' => 'asc'], ['IBLOCK_ID' => $property['IBLOCK_ID'], 'PROPERTY_ID' => $property['ID']]);
        while ($row = $rows->Fetch()) {
            $ret[$row['XML_ID']] = $row['ID'];
        }

        return self::$enums[$code] = $ret;
    }
}