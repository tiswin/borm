<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;

use Bitrix\Catalog\PriceTable;
use Iswin\Borm\Exceptions\ExceptionEntity;
use Iswin\Borm\Iblock\Element\Interfaces\CatalogPriceInterface;

/**
 * Объект - цена элемента каталога
 *
 * Class CatalogPrice
 * @package Iswin\Borm\Iblock\Element
 */
class CatalogPrice implements CatalogPriceInterface
{
    protected $data;
    protected $productId;

    protected function __construct ($productId, $data = [])
    {
        $this->data = $data;
        $this->productId = $productId;
    }

    /**
     * @param $productId
     * @param array $data
     * @return CatalogPrice
     */
    public static function getInstance($productId, $data = [])
    {
        return new static($productId, $data);
    }

    /**
     * Устанавливает ID продукта
     *
     * @param $productId
     * @return $this
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     * Возвращает ID продукты
     *
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Устанавливает значение поля цены по его коду
     *
     * @param $key
     * @param $val
     * @return $this
     */
    public function setField($key, $val)
    {
        $this->data[$key] = $val;
        return $this;
    }

    /**
     * Возвращает значение цены
     *
     * @return mixed
     */
    public function getPrice()
    {
        return $this->data['PRICE'];
    }

    /**
     * Возвращает валюту цены
     *
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->data['CURRENCY'];
    }

    /**
     * Это базовая цена?
     *
     * @return bool
     */
    public function isBase()
    {
        return $this->data['TYPE']['BASE'] == 'Y';
    }

    /**
     * Вовзращает название типа цены
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->data['TYPE']['NAME'] ? : $this->data['GROUP_NAME'];
    }

    /**
     * Возвращает ID типа цены
     *
     * @return mixed
     */
    public function getPriceGroupId()
    {
        return $this->data['GROUP_ID'] ? : $this->data['TYPE']['ID'];
    }

    /**
     * Устанавливает тип цены
     *
     * @param $groupId
     * @return $this
     */
    public function setPriceGroupId($groupId)
    {
        $this->data['GROUP_ID'] = $groupId;
        return $this;
    }

    /**
     * Возвращает ID цены
     *
     * @return mixed
     */
    public function getPriceId()
    {
        return $this->data['PRICE_ID'] ? : $this->data['ID'];
    }

    /**
     * Устанаваливает ID цены
     *
     * @param $id
     * @return $this
     */
    public function setPriceId($id)
    {
        $this->data['PRICE_ID'] = $this->data['ID'] = $id;
        return $this;
    }

    /**
     * Устанавливает тип цены
     *
     * @param $type - ассцоиативный массив с полями типа цены
     * @return $this
     */
    public function setType($type)
    {
        $this->data['TYPE'] = $type;
        return $this;
    }

    /**
     * Устанаваливает значение цены
     *
     * @param $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->data['PRICE'] = $price;
        return $this;
    }

    /**
     * Устанавливает валюту
     *
     * @param $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->data['CURRENCY'] = $currency;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getExtraId()
    {
        return $this->data['EXTRA_ID'];
    }

    /**
     * @return mixed
     */
    public function getQuantityFrom()
    {
        return $this->data['QUANTITY_FROM'];
    }

    /**
     * @return mixed
     */
    public function getQuantityTo()
    {
        return $this->data['QUANTITY_TO'];
    }

    /**
     * Сохраняет цену
     *
     * @throws ExceptionEntity
     * @return $this
     */
    public function save()
    {
        $priceId = $this->getPriceId();
        $data = [
            'PRODUCT_ID' => $this->getProductId(),
            'EXTRA_ID' => $this->getExtraId(),
            'CATALOG_GROUP_ID' => $this->getPriceGroupId(),
            'PRICE' => $this->getPrice(),
            'PRICE_SCALE' => $this->getPrice(),
            'CURRENCY' => $this->getCurrency(),
            'QUANTITY_FROM' => $this->getQuantityFrom(),
            'QUANTITY_TO' => $this->getQuantityTo()
        ];

        if ($priceId) {
            $res = PriceTable::update($priceId, $data);
        } else {
            $res = PriceTable::add($data);
            $priceId = $res->getId();
            $this->setPriceId($priceId);
        }

        if (!$res->isSuccess()) {
            $message = implode("; ", $res->getErrorMessages());
            $e = new ExceptionEntity($message);
            throw $e->setData($data);
        }

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function canAccess()
    {
        return $this->data['CAN_ACCESS'] == 'Y';
    }

    public function canBuy()
    {
        return $this->data['CAN_BUY'] == 'Y';
    }
}