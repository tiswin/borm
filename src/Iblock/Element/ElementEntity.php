<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;

use Bitrix\Main\Loader;
use Bitrix\Catalog\GroupTable as PriceTypeTable;
use Iswin\Borm\Common\Collection;
use Iswin\Borm\HlBlock\HighLoadEntity;
use Iswin\Borm\HlBlock\HighLoadEntityInterface;
use Iswin\Borm\Iblock\Element\Interfaces\CatalogDataInterface;
use Iswin\Borm\Iblock\IblockEntity;
use Iswin\Borm\Iblock\Section\SectionEntity;

Loader::includeModule('catalog');

/**
 * Class ElementEntity
 * @package Iswin\Borm\Iblock\Element
 */
abstract class ElementEntity extends IblockEntity implements ElementEntityInterface
{
    protected $sectionIds;

    public static function getRepository ()
    {
        return ElementRepository::getInstance(static::getIblockCode());
    }

    /**
     * @see ElementQuery
     *
     * @return ElementQuery
     */
    public static function query ()
    {
        $query = ElementQuery::getInstance(static::getInstance());
        $query->setProperties(static::getProperties());
        return $query;
    }

    protected static $saveInstance = false;

    /**
     * @return \CIBlockElement
     */
    protected function getSaveInstance()
    {
        if (self::$saveInstance !== false) {
            return self::$saveInstance;
        }

        return self::$saveInstance = new \CIBlockElement();
    }

    /**
     * Обновляет или добавляет новый элемент
     * Если ID утсановлен - происходит обновление,
     * В противном случае добавление.
     * Также сохраняет значения всех свойств и создает продукт
     * Внимание! Данные метод не сохраняет поля каталога, их нужно сохранять отдельно:
     * <code>
     *      $elementBase->getCatalogData()->save();
     * </code>
     * Чтобы продукт был добавлен в ProductTable
     * @see CatalogData::save()
     *
     * @return $this
     */
    public function save() {
        $instance = $this->getSaveInstance();
        $this->saveByInstance($instance);

        if ($props = $this->getPropsCollection()) {
            $props->setElementId($this->getId());
            $props->save();
        }
        if ($catalog = $this->getCatalogData()) {
            $catalog->setProductId($this->getId());
        }
        $instance
            ->SetElementSection($this->getId(), $this->getSectionIds());

        return $this;
    }

    /**
     * Возвращает ID разделов в которых будет находиться элемент
     *
     * @param $ids
     * @return $this
     */
    public function setSectionIds($ids)
    {
        $this->sectionIds = $ids;
        return $this;
    }

    /**
     * Возвращает ID разделов в которых находится элемент
     *
     * @return array
     */
    public function getSectionIds()
    {
        return $this->sectionIds ? : [$this->getSectionId()];
    }

    protected function getDataForSave ()
    {
        $data = parent::getDataForSave();
        unset($data['CATALOG_DATA']);
        unset($data['PROP_COLLECTION']);

        if (is_numeric($data['PREVIEW_PICTURE'])) {
            unset($data['PREVIEW_PICTURE']);
        }

        if (is_numeric($data['DETAIL_PICTURE'])) {
            unset($data['DETAIL_PICTURE']);
        }

        return $data;
    }



    /**
     * Возвращает коллекцию значений свойств установленных извне
     *
     * @return PropsCollection
     */
    public function getPropsCollection()
    {
        return $this->data['PROP_COLLECTION'] ? :
            $this->data['PROP_COLLECTION'] = PropsCollection::getInstance(
                $this->getId(),
                $this->getIblockId(),
                static::getRepository()->getPropertiesByCodes()
            );
    }

    protected static $priceTypes = false;

    /**
     * Возвращает все типы цен на сайте
     * @todo добавить учет уровня доступа к типам цен
     *
     * @return array
     */
    public static function getPriceTypes()
    {
        if (static::$priceTypes !== false) {
            return static::$priceTypes;
        }

        self::$priceTypes = $ret = [];

        $rows = PriceTypeTable::query()
            ->addSelect('*')
            ->exec();

        while ($row = $rows->fetch()) {
            $ret[$row['ID']] = $row;
        }

        return static::$priceTypes = Collection::getInstance($ret);
    }

    protected static $basePriceType = false;

    /**
     * Возвращает базовый тип цен
     *
     * @return array|bool|mixed
     */
    public static function getBasePriceType()
    {
        if (static::$basePriceType !== false) {
            return static::$basePriceType;
        }

        $priceTypes = static::getPriceTypes();
        foreach ($priceTypes as $priceType) {
            if ($priceType['BASE'] == 'Y') {
                return $priceType;
            }
        }

        return [];
    }

    /**
     * Возвращает объект каталога, для операций с продуктом и ценами
     * Внимание! если элемент инфоблока новый, или для него не создан продукт в каталоге
     * метод вернет null, если нужно все-таки создать продукт в каталоге, то используйте следующий код как пример
     * <code>
     *      $elementBase
     *              ->makeCatalogData() //создаем пустой инстанс данных каталога для продукта
     *              ->setProductTypeWithoutOffers() //устанавливаем тип продукта
     *              ->setPriceTypeSingle() //устанавливаем тип цены продукта
     *              ->save(); //создаем продукт в каталоге
     * </code>
     *
     * @return CatalogData|null
     */
    public function getCatalogData()
    {
        return $this->data['CATALOG_DATA'];
    }

    /**
     * Создает новый объект каталога
     *
     * @param $data
     * @return CatalogData
     */
    public function makeCatalogData($data = [])
    {
        return $this->data['CATALOG_DATA'] = CatalogData::getInstance($data, $this->getId());
    }

    /**
     * Устанавливает объект каталога
     *
     * @param CatalogDataInterface $data
     * @return $this
     */
    public function setCatalogData(CatalogDataInterface $data)
    {
        $this->data['CATALOG_DATA'] = $data;
        return $this;
    }

    /**
     * Устанавливает описание для анонса
     *
     * @param $text
     * @return $this
     */
    public function setPreviewText($text)
    {
        $this->data['PREVIEW_TEXT'] = $text;
        return $this;
    }

    /**
     * Устанавливает детальное описание
     *
     * @param $text
     * @return $this
     */
    public function setDetailText($text)
    {
        $this->data['DETAIL_TEXT'] = $text;
        return $this;
    }

    /**
     * Возвращает ID изображения анонса
     *
     * @return mixed
     */
    public function getPreviewImageId()
    {
        return $this->data['PREVIEW_PICTURE'];
    }

    /**
     * Устанавливает изображение для анонса по ID  файла
     *
     * @param $id
     * @return $this
     */
    public function setPreviewImageId($id)
    {
        $this->data['PREVIEW_PICTURE'] = \CFile::MakeFileArray($id);
        return $this;
    }

    /**
     * Возвращает ID детального изображения
     *
     * @return mixed
     */
    public function getDetailImageId()
    {
        return $this->data['DETAIL_PICTURE'];
    }

    /**
     * Устанавливает детальное изображение по ID файла
     *
     * @param $id
     * @return $this
     */
    public function setDetailImageId($id)
    {
        $this->data['DETAIL_PICTURE'] = \CFile::MakeFileArray($id);
        return $this;
    }

    protected function getDirectoryXmlIds($code)
    {
        return $this->getPropsCollection()->getValue($code) ? : [];
    }

    protected function addDirectory(HighLoadEntityInterface $entity, $code)
    {
        $xmlIds = $this->getDirectoryXmlIds($code);
        $xmlId = $entity->getXmlId();
        if ($xmlId && !in_array($xmlId, $xmlIds)) {
            $xmlIds[] = $xmlId;
            $this->getPropsCollection()->setValue($code, $xmlIds);
        }
        return $this;
    }

    /**
     * @param HighLoadEntityInterface[] $entities
     * @param $code
     */
    protected function setDirectoryValues($entities, $code)
    {
        $xmlIds = [];

        foreach ($entities as $entity) {

            if (!is_object($entity)) {
                continue;
            }

            $xmlId = $entity->getXmlId();
            if (!in_array($xmlId, $xmlIds)) {
                $xmlIds[] = $entity->getXmlId();
            }
        }

        $this->getPropsCollection()->setValue($code, $xmlIds);
    }

    /**
     * Возвращает объект раздела элемента инфоблока,
     * если он был запрошен в параметрах
     *
     * @see ElementQuery::selectSection()
     *
     * @return SectionEntity|null
     */
    public function getSection()
    {
        return $this->getOneLinkObjectByCode(ElementQuery::SECTION_LINK_CODE);
    }
}