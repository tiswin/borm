<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock\Element;

use Iswin\Borm\Iblock\IblockEntityInterface;

interface ElementEntityInterface extends IblockEntityInterface
{
    /**
     * @return PropsCollection
     */
    public function getPropsCollection();

    /**
     * @return CatalogData
     */
    public function getCatalogData();
}