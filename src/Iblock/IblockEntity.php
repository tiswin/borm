<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;
use Iswin\Borm\Common\Collection;
use Iswin\Borm\Common\EntityInterface;
use Iswin\Borm\Exceptions\ExceptionEntity;
use Iswin\Borm\Iblock\Repository\IblockRepositoryInterface;


/**
 * Базовый класс для объектов - элементов и разделов инфоблока
 *
 * Class IblockEntityBase
 */
abstract class IblockEntity implements IblockEntityInterface
{
    /**
     * Метод должен возвращать код инфоблока, к которому относится сущность
     *
     * @return mixed
     */
    abstract public static function getIblockCode();

    /**
     * Метод должен сохранять сущность в БД
     * @see ElementBase
     *
     * @return $this
     */
    abstract public function save();

    /**
     * Должен возвращать репозиторий для запросов в инфоблоки
     *
     * @return IblockRepositoryInterface
     */
    abstract public static function getRepository();


    protected $data;

    protected $linkedObjects = [];

    protected function __construct ($data)
    {
        $this->data = $data;
    }

    /**
     * @param array $data
     * @return static
     */
    public static function getInstance($data = [])
    {
        return new static($data);
    }

    /**
     * Возвращает ID объекта
     * @return mixed
     */
    public function getId()
    {
        return $this->data['ID'];
    }

    /**
     * Устанавливает имя объекта
     *
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->data['NAME'] = $name;
        return $this;
    }

    /**
     * Возвращает имя объекта
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->data['NAME'];
    }

    /**
     * Устанавливает код объекта
     *
     * @param $code
     * @return $this
     */
    public function setCode($code)
    {
        $this->data['CODE'] = $code;
        return $this;
    }

    /**
     * Устанавливает сортировку объекта
     *
     * @param $sort
     * @return $this
     */
    public function setSort($sort)
    {
        $this->data['SORT'] = $sort;
        return $this;
    }

    /**
     * Возвращает код объекта
     *
     * @return string
     */
    public function getCode()
    {
        return $this->data['CODE'];
    }

    /**
     * Объект активен?
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->data['ACTIVE'] == 'Y';
    }

    /**
     * Активирует элемент
     *
     * @return $this
     */
    public function enable()
    {
        $this->data['ACTIVE'] = 'Y';
        return $this;
    }

    /**
     * Деактивиурет объект
     *
     * @return $this
     */
    public function disable()
    {
        $this->data['ACTIVE'] = 'N';
        return $this;

    }

    /**
     * Устанавливает родительский раздел
     *
     * @param $sectionId
     * @return $this
     */
    public function setSectionId($sectionId)
    {
        $this->data['IBLOCK_SECTION_ID'] = $sectionId;
        return $this;
    }

    /**
     * Возвращает родительский раздел
     *
     * @return int|null
     */
    public function getSectionId()
    {
        return $this->data['IBLOCK_SECTION_ID'];
    }

    /**
     * Генерирует символьный код из название и устанавливает его
     *
     * @return IblockEntity
     */
    public function makeCode()
    {
        $code = \CUtil::translit($this->getName(), 'ru');
        return $this->setCode($code);
    }

    /**
     * Возвращает символьный код для импортируемых объектов
     *
     * @param $id
     * @return null|string
     */
    public function getImportCode($id)
    {
        $code = \CUtil::translit($this->getName() . '-' . $id, 'ru', ['replace_space' => '-', 'replace_other' => '-']);
        return $code;
    }

    /**
     * Возвращает ID инфоблока объекта
     *
     * @return int
     */
    public function getIblockId()
    {
        return static::getRepository()->getId();
    }

    /**
     * Возвращает все поля объекта
     *
     * @return array
     */
    public function getData()
    {
        $data = $this->data;
        $data['IBLOCK_ID'] = static::getRepository()->getId();
        return $data;
    }


    /**
     * @see EntityInterface::setData()
     *
     * @param $data
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    protected function getDataForSave()
    {
        if (!$this->getCode()) {
            $this->makeCode();
        }

        $data = $this->getData();
        unset($data['ID']);
        unset($data['TIMESTAMP_X_UNIX']);
        unset($data['TIMESTAMP_X']);
        unset($data['MODIFIED_BY']);
        unset($data['DATE_CREATE_UNIX']);

        foreach ($data as $key => $val) {
            if ($key[0] == '~') {
                unset($data[$key]);
            }
        }

        return $data;
    }

    /**
     * Возвращает значение любого поля по его коду
     *
     * @param $code
     * @return mixed
     */
    public function getField($code)
    {
        return $this->data[$code];
    }

    /**
     * Устанавливает ID объекта
     *
     * @param $id
     * @return $this
     */
    public function setId($id)
    {
        $this->data['ID'] = $id;
        return $this;
    }


    /**
     * Возвращает объект для формирования и осуществления выборки
     *
     * @return Query
     */
    public static function query()
    {
        return Query::getInstance(static::getInstance());
    }

    /**
     * @param $rows
     * @return EntityInterface[]
     */
    protected static function makeObjectsFromArray($rows)
    {
        $ret = [];
        /** @var IblockEntity $className */
        $className = get_called_class();
        foreach ($rows as $row) {
            $ret[] = $className::getInstance($row);
        }

        return $ret;
    }

    /**
     * Возвращает объект по его ID
     *
     * @param $id
     * @param array $select
     * @return EntityInterface|null
     */
    public static function getById($id, $select = [])
    {
        if (!$id) {
            return null;
        }

        return
            static::query()
                ->setSelect($select)
                ->addFilter('=ID', $id)
                ->getOneResult();
    }

    /**
     * Возвращает объекты по массиву ID
     *
     * @param $ids
     * @param array $select
     * @param array $order
     * @return EntityInterface[]
     */
    public static function getByIds($ids, $select = [], $order = [])
    {
        static::validateFilterArray($ids);
        if (!$ids) {
            return [];
        }

        return
            static::query()
                ->setSelect($select)
                ->addFilter('=ID', $ids)
                ->setOrder($order)
                ->fetchAll();
    }

    protected static function validateFilterArray(&$ids)
    {
        foreach ($ids as $index => $id) {
            $id = trim($id);
            if (!$id) {
                unset($ids[$index]);
            }
        }
    }

    /**
     * @param \CIBlockElement|\CIBlockSection $instance
     * @return $this
     * @throws ExceptionEntity
     */
    protected function saveByInstance($instance)
    {
        $id = $this->getId();
        $saveData = $this->getDataForSave();
        if ($id) {
            $res = $instance->Update($id, $saveData);
        } else {
            $res = $instance->Add($saveData);
            $this->setId($res);
        }

        if (!$res) {
            $e = new ExceptionEntity($instance->LAST_ERROR);
            $saveData['ID'] = $id;
            throw $e->setData($saveData);
        }

        return $this;
    }


    protected static $properties = [];

    /**
     * Возвращает список свойств инфоблока
     *
     * @return array|bool|mixed
     */
    public static function getProperties()
    {
        $iblockCode = static::getIblockCode() ? : '0';

        if (isset(self::$properties[$iblockCode])) {
            return self::$properties[$iblockCode];
        }

        $ret = [];
        $props = static::getRepository()->getProperties();
        foreach ($props as $prop) {
            $ret[$prop['ID']] = $prop;
        }

        return self::$properties[$iblockCode] = Collection::getInstance($ret);
    }

    protected static $propertiesByCode = [];

    /**
     * Возвращает массив свойства инфоблока по его коду
     *
     * @param $code
     * @return mixed
     */
    public static function getPropertyByCode($code)
    {
        return static::getRepository()->getPropertiesByCodes()[$code];
    }

    /**
     * Возвращает список связанных объектов
     *
     * @return EntityInterface[]
     */
    public function getLinkedObjects()
    {
        return $this->linkedObjects;
    }

    /**
     * Устанавливает связанные объекты
     * @internal пока непонятно нужен ли метод
     *
     * @param $objects
     * @return $this
     */
    protected function setLinkedObjects($objects)
    {
        $this->linkedObjects = $objects;
        return $this;
    }

    /**
     *  Добавляет связанный объект
     *
     * @param $object
     * @return $this
     */
    public function addLinkedObject($code, $object)
    {
        if (!isset($this->linkedObjects[$code])) {
            $this->linkedObjects[$code] = [];
        }

        $this->linkedObjects[$code] []= $object;
        return $this;
    }

    /**
     * Возвращает связанные объекты,
     * ВНИМАНИЕ! Всегда возвращается массив, даже если свойство не множественное
     *
     * @param $code
     * @return EntityInterface[]
     */
    public function getLinkObjectByCode($code)
    {
        return $this->linkedObjects[$code];
    }

    /**
     * Возвращает один связанный объект по его коду
     * Вызывается если заведомо известно, что свойство связи не множественное
     *
     * @param $code
     * @return EntityInterface
     */
    public function getOneLinkObjectByCode($code)
    {
        return current($this->getLinkObjectByCode($code));
    }

    public function setField ($propCode, $value)
    {
        $this->data[$propCode] = $value;
        return $this;
    }
}