<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;

use Iswin\Borm\Common\Finder as RepoFinder;
use Iswin\Borm\Iblock\Element\ElementEntity;
use Iswin\Borm\Iblock\Element\ElementEntityInterface;
use Iswin\Borm\Iblock\Section\SectionEntity;
use Iswin\Borm\Iblock\Section\SectionEntityInterface;

/**
 * Сервис для поиска репозиториев инофблока
 *
 * Class EntityRepoFinder
 * @package Iswin\Borm\Iblock
 */
class Finder extends RepoFinder
{

    /**
     * путь к репозиториям инфоблоков относительно корня модуля
     */
    const ENTITIES_DIR = '/lib/entities/iblock/';

    /**
     * @return FinderResult
     */
    protected function getIblockRepo()
    {
        if ($this->result !== false) {
            return $this->result;
        }

        return $this->result = FinderResult::getInstance();
    }



    /**
     * @param $modulePath
     * @param $iblockCode
     */
    protected function searchInModule($modulePath, $iblockCode)
    {
        $modulePath .= static::ENTITIES_DIR;
        if (!file_exists($modulePath)) {
            return;
        }
        $this->recursiveSearchInPath($modulePath, $iblockCode);
    }

    /**
     * @param IblockEntity $className
     * @param $code
     */
    protected function checkResult($className, $code)
    {
        if (!method_exists($className, 'getIblockCode')) {
            return;
        }

        if (strtolower($className::getIblockCode()) == strtolower($code)) {
            $repo = $this->getIblockRepo();
            $instance = $className::getInstance();
            if ($instance instanceof ElementEntityInterface) {
                $repo->setElementEntity($instance);
            } elseif($instance instanceof SectionEntityInterface) {
                $repo->setSectionEntity($instance);
            }
        }
    }

}