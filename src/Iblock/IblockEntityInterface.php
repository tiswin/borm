<?php
/**
 * Created by Iswin.
 * User: vtaneev
 */

namespace Iswin\Borm\Iblock;


use Iswin\Borm\Common\EntityInterface;
use Iswin\Borm\Iblock\Repository\IblockRepositoryInterface;

interface IblockEntityInterface extends EntityInterface
{
    /**
     * @return IblockRepositoryInterface
     */
    public static function getRepository();

    public function getLinkObjectByCode($code);
}